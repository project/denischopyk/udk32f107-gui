const unsigned int up[100]={0x0007, 0x0007,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0x1F,   0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0x1F,   0x1F,   0x1F,   0xFFFF, 0xFFFF, 
0xFFFF, 0x1F,   0x1F,   0x1F,   0x1F,   0x1F,   0xFFFF, 
0x1F,   0x1F,   0x1F,   0x1F,   0x1F,   0x1F,   0x1F, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF 
};

const unsigned int down[100]={ 0x0007, 0x0007,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 
0xFFFF, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0x1F, 0x1F, 0x1F, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800,
0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF,
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF  
};

const unsigned int left[100]={ 0x0007, 0x0007,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0x1F, 0x1F, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x1F, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF  
};

const unsigned int right[100]={ 0x0007, 0x0007,
0xFFFF, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0x1F, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0x1F, 0x1F, 0x1F, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0x1F, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0x1F, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF 
};

const unsigned int all[164]={ 0x0009, 0x0009,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0x400, 
0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0x400, 0x400, 
0x400, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0x400, 0x400, 0xFFFF, 
0x400, 0x400, 0x400, 0xFFFF, 0x400, 0x400, 0x400, 0xFFFF, 0xFFFF, 
0xFFFF, 0x400, 0x400, 0x400, 0x400, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0x400, 0x400, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0xFFFF, 
0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0xFFFF, 0x400, 
0xFFFF, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 
0x400, 0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 0xFFFF, 
0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0x400, 0xFFFF, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0x400, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF
};

const unsigned int cancel[164]={ 0x0009, 0x0009,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 
0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 
0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 
0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 
0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF   
};

const unsigned int no[164]={ 0x0009, 0x0009,
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 
0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 
0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 
0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 
0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 
0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF,
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 
0xFFFF, 0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 
0xF800, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 
0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 
0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 
0xF800, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 
0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xF800, 
0xFFFF, 0xF800, 0xFFFF, 0xF800, 0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xFFFF, 
0xFFFF, 0xFFFF, 0xF800, 0xF800, 0xF800, 0xF800, 0xF800, 0xFFFF, 0xFFFF      
};

