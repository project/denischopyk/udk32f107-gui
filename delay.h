#ifndef __DELAY__H
#define __DELAY_H


//#include "platform_config.h"
#include "hsi_config.h"



#if defined SYSCLK_FREQ_HSE
  #define COU 1290
  #define UCOU 13
  #define UMCOU 1
#elif defined SYSCLK_FREQ_20MHz 
  #define COU 3228
  #define UCOU 32
  #define UMCOU 3
#elif defined SYSCLK_FREQ_36MHz
  #define COU 4842
  #define UCOU 48
  #define UMCOU 5
#elif defined SYSCLK_FREQ_48MHz
  #define COU 5783
  #define UCOU 57
  #define UMCOU 6
#elif defined SYSCLK_FREQ_72MHz
 #define COU 6838
  #define UCOU 68
  #define UMCOU 7
#endif


void Delay_ms(vu32 nCount);
void Delay_us10(vu32 nCount);
void Delay_us(vu32 nCount);

#endif /* __DELAY_H */