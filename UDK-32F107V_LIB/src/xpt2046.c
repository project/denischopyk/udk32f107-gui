//------------------------------------------------------------------------------
//            ������� Touch Screen �� ������ ����������� XPT2046
//------------------------------------------------------------------------------
#include "xpt2046.h"
#include "ssd1289.h"


//------------------------------------------------------------------------------
//                       ����������� ������� ������ � ���������
//------------------------------------------------------------------------------
#define TouchScreen_MOSI(a) \
    if (a) \
      GPIOC->BSRR  |=  GPIO_BSRR_BS4; \
        else \
      GPIOC->BRR  |=  GPIO_BRR_BR4 

#define  TouchScreen_SCK(a) \
    if (a) \
      GPIOB->BSRR  |=  GPIO_BSRR_BS0; \
        else \
      GPIOB->BRR  |=  GPIO_BRR_BR0   


                      
  
//#define TouchScreen_MISO    getpin(MISO_PORT, MISO_PIN)
   
//#define TouchScreen_IRQ     getpin(IRQ_PORT, IRQ_PIN)  
//------------------------------------------------------------------------------
// ��������� ��� ��������� �����
//------------------------------------------------------------------------------
typedef struct 
{
  int x;
  int y;
} POINT;
//------------------------------------------------------------------------------
// ��������� ������������� ������� Touchscreen
//------------------------------------------------------------------------------
typedef struct  
{
  long An;
  long Bn;
  long Cn;
  long Dn;
  long En;
  long Fn;
  long Divider;
} MATRIX;
//------------------------------------------------------------------------------
//                        ���������� ������� ��������
//------------------------------------------------------------------------------
static void Delay_uS(int k);          // ������� ���
static void Delay_mS(vu32 k);         // ������� ��
static int SPI_RD();                  // ������ ������ Touch Screen �� SPI
static void SPI_WR(u8 cmd);           // ������ �������� � Touch Screen
static int CalibrationMatrix( POINT *lcdPtr, POINT *touchPtr, MATRIX *matrixPtr); // ���������� � ������� ������������� ������� Touch Screen
static int Read_X();                  // ������ ���������� X �� Touch Screen
static int Read_Y();                  // ������ ���������� Y �� Touch Screen
static void Read_Matrix(void);        // ������ ����� ����������� ������������� ������� �� FLASH  
static void Save_Matrix(void);        // ������ ������������� ������� �� FLASH   
static u8 Compare(void);              // �������� ���������� �������� ��������� � ������������    
static void Get_XY(void);             // ������ ��������� X � Y �� Touch Screen � ����������� 50 ���������

//------------------------------------------------------------------------------
//                       ���������� ���������� ��������
//------------------------------------------------------------------------------
static long xAvg, yAvg;
static long xRealpos, yRealpos;
static MATRIX matrix;

                          
//------------------------------------------------------------------------------
//                     �������� ������� ������������� TouchScreen
//------------------------------------------------------------------------------
			

void TouchScreen_Init() 
{

  
RCC->APB2ENR |= (RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPDEN) ;
 //MOSI // 50 ���// push-pull
  GPIOC->CRL   &= ~GPIO_CRL_MODE4;       //�������� ������� MODE
  GPIOC->CRL   &= ~GPIO_CRL_CNF4;        //�������� ������� CNF
  GPIOC->CRL   |=  GPIO_CRL_MODE4_0;     //�����, 10MHz
  GPIOC->CRL   &= ~GPIO_CRL_CNF4;        //������ ����������, �����������  
  //SCK  // 50 ���// push-pull 
  GPIOB->CRL   &= ~GPIO_CRL_MODE0;       //�������� ������� MODE
  GPIOB->CRL   &= ~GPIO_CRL_CNF0;        //�������� ������� CNF
  GPIOB->CRL   |=  GPIO_CRL_MODE0_0;     //�����, 10MHz
  GPIOB->CRL   &= ~GPIO_CRL_CNF0;        //������ ����������, ����������� 
  // ���� ������� MISO TouchScreen// input floating
  GPIOB->CRL   &= ~GPIO_CRL_MODE1;       //�������� ������� MODE
  GPIOB->CRL   &= ~GPIO_CRL_CNF1;        //�������� ������� CNF
  GPIOB->CRL   |=  GPIO_CRL_CNF1_0;      //���������� ����, ������ ���������
  // ���� ������� IRQ TouchScreen// input floating
  GPIOD->CRH   &= ~GPIO_CRH_MODE13;      //�������� ������� MODE
  GPIOD->CRH   &= ~GPIO_CRH_CNF13;       //�������� ������� CNF
  GPIOD->CRH   |=  GPIO_CRH_CNF13_0;     //���������� ����, ������ ���������
  
  SPI_WR(xpt2046_X);
  Delay_uS(5);
  SPI_RD();
}
//------------------------------------------------------------------------------
//                    ������ �������� � Touch Screen
//------------------------------------------------------------------------------
static void SPI_WR(u8 cmd)
{
  unsigned char i, buf;

  TouchScreen_MOSI(0);
  TouchScreen_SCK(0);
  
  for(i=0;i<8;i++) 
    {
      buf=(cmd>>(7-i))&0x1;
      TouchScreen_MOSI(buf);
      Delay_uS(5);
      TouchScreen_SCK(1);
      Delay_uS(5);
      TouchScreen_SCK(0);
    }
}
//------------------------------------------------------------------------------
//                       ������ ������ Touch Screen �� SPI
//------------------------------------------------------------------------------
static int SPI_RD()
{
  int buf=0,temp;
  unsigned char i;
  TouchScreen_MOSI(0);
  TouchScreen_SCK(1);
  for(i=0;i<12;i++) 
    {
      Delay_uS(5);
      TouchScreen_SCK(0);         
      Delay_uS(5);   
      temp= (TouchScreen_MISO()) ? 1:0;
      buf|=(temp<<(11-i));
        
      Delay_uS(5);
      TouchScreen_SCK(1);
    }
  
  buf&=0x0fff;
  return(buf);
}
//------------------------------------------------------------------------------
//                    ������ ���������� X �� Touch Screen
//------------------------------------------------------------------------------
static int Read_X() 
{
  SPI_WR(xpt2046_X);
  Delay_uS(5);
  return SPI_RD();   
}
//------------------------------------------------------------------------------
//                    ������ ���������s Y �� TouchScreen
//------------------------------------------------------------------------------
static int Read_Y() 
{ 
  SPI_WR(xpt2046_Y);
  Delay_uS(5);
  return SPI_RD(); 
}
//------------------------------------------------------------------------------
//                             ������� k ���
//------------------------------------------------------------------------------
static void Delay_uS(int k)
{
  for(int j=k;j > 0;j--);    
}
//------------------------------------------------------------------------------
//                             ������� k ��
//------------------------------------------------------------------------------
static void Delay_mS(vu32 k)
{
  k *= 2000;
  for(; k != 0; k--);
}
//------------------------------------------------------------------------------
//             ���������� � ������� ������������� ������� Touch Screen
//------------------------------------------------------------------------------
static int CalibrationMatrix( POINT *lcdPtr, POINT *touchPtr, MATRIX *matrixPtr)
{
  int  retValue = 1 ;
  
  matrixPtr->Divider = ((touchPtr[0].x - touchPtr[2].x) * (touchPtr[1].y - touchPtr[2].y)) - 
                       ((touchPtr[1].x - touchPtr[2].x) * (touchPtr[0].y - touchPtr[2].y)) ;
  
  if( matrixPtr->Divider == 0 ) retValue = 0 ;
  else
    {
      matrixPtr->An = ((lcdPtr[0].x - lcdPtr[2].x) * (touchPtr[1].y - touchPtr[2].y)) - 
                      ((lcdPtr[1].x - lcdPtr[2].x) * (touchPtr[0].y - touchPtr[2].y)) ;

      matrixPtr->Bn = ((touchPtr[0].x - touchPtr[2].x) * (lcdPtr[1].x - lcdPtr[2].x)) - 
                      ((lcdPtr[0].x - lcdPtr[2].x) * (touchPtr[1].x - touchPtr[2].x)) ;

      matrixPtr->Cn = (touchPtr[2].x * lcdPtr[1].x - touchPtr[1].x * lcdPtr[2].x) * touchPtr[0].y +
                      (touchPtr[0].x * lcdPtr[2].x - touchPtr[2].x * lcdPtr[0].x) * touchPtr[1].y +
                      (touchPtr[1].x * lcdPtr[0].x - touchPtr[0].x * lcdPtr[1].x) * touchPtr[2].y ;

      matrixPtr->Dn = ((lcdPtr[0].y - lcdPtr[2].y) * (touchPtr[1].y - touchPtr[2].y)) - 
                      ((lcdPtr[1].y - lcdPtr[2].y) * (touchPtr[0].y - touchPtr[2].y)) ;
    
      matrixPtr->En = ((touchPtr[0].x - touchPtr[2].x) * (lcdPtr[1].y - lcdPtr[2].y)) - 
                      ((lcdPtr[0].y - lcdPtr[2].y) * (touchPtr[1].x - touchPtr[2].x)) ;

      matrixPtr->Fn = (touchPtr[2].x * lcdPtr[1].y - touchPtr[1].x * lcdPtr[2].y) * touchPtr[0].y +
                      (touchPtr[0].x * lcdPtr[2].y - touchPtr[2].x * lcdPtr[0].y) * touchPtr[1].y +
                      (touchPtr[1].x * lcdPtr[0].y - touchPtr[0].x * lcdPtr[1].y) * touchPtr[2].y ;
    }
  
  return( retValue ) ;
}
//------------------------------------------------------------------------------
//   ���������� ����� ���������� Touch Screen                          
//------------------------------------------------------------------------------
void TouchScreen_Calibrate(void)
{
  POINT screenSample[3];  //array of input points
  POINT displaySample[3] = {{32,24},{160,215},{287,120}};  //array of expected correct answers  TS.getMatrix();

  LCD_FillScreen(BLACK);
  LCD_WriteString_8x16(65,110, "Touch Screen calibration",WHITE,BLACK);
  while(!TouchScreen_IRQ());  // Loop while touch panel is touched, so we are sure it first start calibrating when not touched
  Delay_mS(2000);
  //LCD_FillScreen(BLACK); 
  //LCD_WriteString_8x16(0,0, "Calibrate", WHITE, BLACK);
    
  for (u8 i=0; i<3; i++)
    {
      // draw touch point    
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y , 4, RED, 0);
 
      while(TouchScreen_IRQ());  // Loop while touch panel is NOT touch
      
      Delay_mS(1);
    
      // got a point
      Get_XY();
      screenSample[i].x = xAvg;
      screenSample[i].y = yAvg;

      // mark complete point
      
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,3, WHITE, 0);
      Delay_mS(100);
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,2, WHITE, 0);
      Delay_mS(100);
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,1, WHITE, 0);
      Delay_mS(300);    
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,4, WHITE, 0);
      Delay_mS(100);
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,3, BLACK, 0);
      Delay_mS(100);
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,2, BLACK, 0);
      Delay_mS(100);
      LCD_Draw_Circle(displaySample[i].x, displaySample[i].y,1, BLACK, 0);
      
      while(!TouchScreen_IRQ());  // Loop while touch panel IS touched
      Delay_mS(250);    
    }
  
  LCD_FillScreen(BLACK);
  LCD_WriteString_8x16(40,110, "Saving Calibration data...", WHITE, BLACK);
  CalibrationMatrix(&displaySample[0], &screenSample[0], &matrix);  // calibration
  Save_Matrix();   // save matrix to eeprom  - not implemented yet
  LCD_FillScreen(BLACK);
  LCD_WriteString_8x16(110,110, "Calibrated", WHITE, BLACK);
  Delay_mS(1000);
}
//------------------------------------------------------------------------------
//   ������ ��������� X � Y �� TouchScreen � ����������� 50 ���������
//------------------------------------------------------------------------------
void Get_XY(void)
{
  u8 count = 0;
  xAvg = Read_Y();
  yAvg = Read_X();

  while(!TouchScreen_IRQ() && count <= 50)
    {
      xAvg += Read_Y();
      xAvg >>= 1;
      yAvg += Read_X();
      yAvg >>= 1;
      Delay_uS(100);
      count++;     
    }
}    
//------------------------------------------------------------------------------
//    �������� ���������� �������� ��������� � ������������                   
//------------------------------------------------------------------------------
u8 Compare(void)
{
  Get_XY();
  if (xRealpos != xAvg || yRealpos != yAvg) return 0;
  else 
    {
      Get_XY();
      if (xRealpos != xAvg || yRealpos != yAvg) return 0;
      else return 1; 
    }
}
//------------------------------------------------------------------------------
//     ������ ������ Touch Screen c ����������� �� ����������                        
//------------------------------------------------------------------------------
void TouchScreen_Read(int *X, int *Y)
{
    Get_XY();
    xRealpos = xAvg;
    yRealpos = yAvg;
    while(Compare == 0) {}
	// calculate matrix
    *X = ((matrix.An * xRealpos) + (matrix.Bn * yRealpos) + matrix.Cn) / matrix.Divider;
    *Y = ((matrix.Dn * xRealpos) + (matrix.En * yRealpos) + matrix.Fn) / matrix.Divider;
}
//------------------------------------------------------------------------------
//   ������ ����� ����������� ������������� ������� �� FLASH                         
//------------------------------------------------------------------------------
static void Read_Matrix(void)
{
    int *ptr; // Create a pointer
    ptr = (int *) &matrix; // Set the pointer to the adress of the MATRIX variable

    for (int i = 0; i < sizeof(MATRIX)/2; i++) 
      {
        *ptr = (*(__IO uint16_t*)(MATRIX_FLASH_ADDR+(i*2)));
        ptr++;
      }
}
//------------------------------------------------------------------------------
//   ������ ������������� ������� �� FLASH                              
//------------------------------------------------------------------------------
static void Save_Matrix(void)
{
  //SaveSettingsToFlash();
}
//------------------------------------------------------------------------------

uint8_t TouchScreen_IRQ(void)
{
  uint8_t bitstatus = 0x00;
 
   if ((GPIOD->IDR & GPIO_IDR_IDR13) != (uint32_t)Bit_RESET)
  {
    bitstatus = (uint8_t)Bit_SET;
  }
  else 
  {
    bitstatus = (uint8_t)Bit_RESET;
  }
  return bitstatus;
}


uint8_t TouchScreen_MISO(void)
{
  uint8_t bitstatus = 0x00;
 
  if ((GPIOB->IDR & GPIO_IDR_IDR1) != (uint32_t)Bit_RESET)

  {
    bitstatus = (uint8_t)Bit_SET;
  }
  else   
  {
    bitstatus = (uint8_t)Bit_RESET;
  }
  return bitstatus;
}
//-----------------------------------------------------------------------------