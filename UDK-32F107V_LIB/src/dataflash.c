//------------------------------------------------------------------------------
//          ������� Dataflash
//------------------------------------------------------------------------------
#include "dataflash.h"
//------------------------------------------------------------------------------
//  ���� ������� ��� ����������� �������������
//------------------------------------------------------------------------------
#define FlashPageRead			0x52	// Main memory page read
#define ContArrayRead			0x68	// Continuous Array Read (Note : Only A/B/D-parts supported)
#define StatusReg			0x57	// Status register
#define Buf1Read			0x54	// Buffer 1 read
#define Buf2Read			0x56	// Buffer 2 read
#define Buf1Write			0x84	// Buffer 1 write
#define Buf2Write			0x87	// Buffer 2 write

#define DF_STATUS_READY                 0x80    // ���� ���������� Dataflash
//------------------------------------------------------------------------------
#define Dataflash_Select()    GPIO_ResetBits(Dataflash_CS_GPIO_PORT, Dataflash_CS_PIN) /*  CS \ */
#define Dataflash_Deselect()  GPIO_SetBits(Dataflash_CS_GPIO_PORT, Dataflash_CS_PIN)   /*  CS / */
//------------------------------------------------------------------------------
// ��������� ���������� Dataflash
//------------------------------------------------------------------------------
#ifdef DATAFLASH_AUTODETECT
TDataflash_Info Dataflash_Info;
const TDataflash_Info Dataflash_Table [] =
{
   { 512,  264,  9,  (0x3<<2) }, // 1M  AT45DB011
   { 1024, 264,  9,  (0x5<<2) }, // 2M  AT45DB021
   { 2048, 264,  9,  (0x7<<2) }, // 4M  AT45DB041
   { 4096, 264,  9,  (0x9<<2) }, // 8M  AT45DB081
   { 4096, 528,  10, (0xB<<2) }, // 16M AT45DB161
   { 8192, 528,  10, (0xD<<2) }, // 32M AT45DB321
   { 8192, 1056, 11, (0xF<<2) }, // 64M AT45DB642
};
#else // DATAFLASH_AUTODETECT
#warning "��������������� Dataflash ���������!"
static const TDataflash_Info Dataflash_Info = { 8192, 528,  10, (0xD<<2) }; // 32M AT45DB321
#endif
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// ��������� �������
//------------------------------------------------------------------------------
static u8 Dataflash_SPI_RW(u8 TX); //  �������� ��������� � ���������� �������� ����
static u8 Dataflash_GetStatus();        //  ������ �������� �������
static result Dataflash_Detect();
//------------------------------------------------------------------------------
// ������������� Dataflash 
//------------------------------------------------------------------------------
result Dataflash_Init()
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  SPI_InitTypeDef   SPI_InitStructure;
  
  RCC_APB2PeriphClockCmd(Dataflash_CS_GPIO_CLK |                      // �������� ������������ GPIOA, GPIOB, SPI1 � AFIO 
                         Dataflash_SPI_MOSI_GPIO_CLK | 
                         Dataflash_SPI_MISO_GPIO_CLK |
                         Dataflash_SPI_SCK_GPIO_CLK | 
                         RCC_APB2Periph_AFIO |
                         Dataflash_SPI_CLK, ENABLE);

  GPIO_PinRemapConfig(Dataflash_SPI_Remap, Dataflash_SPI_PINS_Remap); // ��������� Remap ������� ���� SPI, ���� ����� 
  
  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_SCK_PIN;                // ����� SCK -
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;                   // 50 ���
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;                     // alternative function, push-pull
  GPIO_Init(Dataflash_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);            

  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_MOSI_PIN;               // ����� MOSI - 50 ��� alternative function, push-pull
  GPIO_Init(Dataflash_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_MISO_PIN;               // ����� MISO -
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;               // input floating
  GPIO_Init(Dataflash_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = Dataflash_CS_PIN;                     // ����� CS - 50 ���
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;                    // output push-pull
  GPIO_Init(Dataflash_CS_GPIO_PORT, &GPIO_InitStructure);

  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  // 2 �����, ������ �������
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;                       // SPI Master
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;                   // � ������� 8 ���
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;                         // � ���������� ��������� SCK = 1
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;                        // ������ �������� �� ������� ��������
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;                           // ����������� �������� NSS
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;  // ����������� ������������ = 2
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;                  // ������ ���������� ������� ���
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(Dataflash_SPI, &SPI_InitStructure);
  SPI_Cmd(Dataflash_SPI, ENABLE);                                     // �������� SPI 
  
  return (Dataflash_Detect());
}
//------------------------------------------------------------------------------
// ��������������� Dataflash 
//------------------------------------------------------------------------------
void Dataflash_Deinit()
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  RCC_APB2PeriphClockCmd(Dataflash_CS_GPIO_CLK |                      // ��������� ������������ GPIOA, GPIOB, SPI1 � AFIO 
                         Dataflash_SPI_MOSI_GPIO_CLK | 
                         Dataflash_SPI_MISO_GPIO_CLK |
                         Dataflash_SPI_SCK_GPIO_CLK | 
                         RCC_APB2Periph_AFIO |
                         Dataflash_SPI_CLK, DISABLE);

  GPIO_PinRemapConfig(Dataflash_SPI_Remap, DISABLE);                  // ��������� Remap ������� ���� SPI
  
  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_SCK_PIN;                // ����� SCK -
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;               // input floating
  GPIO_Init(Dataflash_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);            

  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_MOSI_PIN;               // ����� MOSI - input floating
  GPIO_Init(Dataflash_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = Dataflash_SPI_MISO_PIN;               // ����� MISO -  input floating
  GPIO_Init(Dataflash_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = Dataflash_CS_PIN;                     // ����� CS -  input floating
  GPIO_Init(Dataflash_CS_GPIO_PORT, &GPIO_InitStructure); 
    
  SPI_Cmd(Dataflash_SPI, DISABLE);                                    // ��������� SPI �
  SPI_I2S_DeInit(Dataflash_SPI);                                      // ���������������� ���
}
//------------------------------------------------------------------------------------
//  ������������ ��� dataflash
//------------------------------------------------------------------------------------
static result Dataflash_Detect()
{
#ifdef DATAFLASH_AUTODETECT
  u8 chip_id;
  
  chip_id = Dataflash_GetStatus() & 0x3C;
  
  for (u8 i=0; i<7; i++)
    {
      if (Dataflash_Table[i].Chip_ID == chip_id )
        {
          Dataflash_Info = Dataflash_Table[i];
          return OK;
        }
    }
  return ERR;
#else // DATAFLASH_AUTODETECT
  return OK;
#endif // DATAFLASH_AUTODETECT
}
//------------------------------------------------------------------------------
//  ���������, ������ �� Dataflash
//------------------------------------------------------------------------------
u8 Dataflash_isReady()
{
  return (Dataflash_GetStatus() &  DF_STATUS_READY);
}
//------------------------------------------------------------------------------
//  ������ ���� ��������� Dataflash. � ���������� ����� ����� ����� ������ �
//  ������� STATUS REGISTER READ.
//------------------------------------------------------------------------------
static u8 Dataflash_GetStatus()
{
  u8 status;
  
  Dataflash_Select();
  status = Dataflash_SPI_RW(StatusReg);
  status = Dataflash_SPI_RW(0x00);
  Dataflash_Deselect();
  
  return status;
}

//------------------------------------------------------------------------------------
//  ������ ���� ���� �� SRAM-������ dataflash
//	���������: BuferNo -> ����� ������ (1 ��� 2)
//                  Addr  -> ����� � ������
//------------------------------------------------------------------------------------
u8 Dataflash_GetByte(u8 BuferNo, u16 Addr )
{
  u8 data;
  
  Dataflash_Select();
  
  if ( BuferNo == 1 ) Dataflash_SPI_RW(Buf1Read);        // ������ 1
  else	if ( BuferNo == 2 )  Dataflash_SPI_RW(Buf2Read); // ������ 2
  else
    { 
      Dataflash_Deselect(); // �������� � ������� �������
      return 0; 
    }                      
  
  Dataflash_SPI_RW(0x00);              
  Dataflash_SPI_RW((u8)(Addr>>8));
  Dataflash_SPI_RW((u8)(Addr));
  Dataflash_SPI_RW(0x00);
  data = Dataflash_SPI_RW(0x00);
  
  Dataflash_Deselect();
  return data;
}
//------------------------------------------------------------------------------------
//  ������ ���� ������ �� SRAM-������ dataflash
//	���������: BuferNo   -> ����� ������ (1 ��� 2)
//                  Addr     -> ����� � ������ � �������� �������� ������
//                  Count    -> ���������� ����, ������� ���������� ���������
//  		   *BuferPtr -> ����� ������, � ������� �������� ������
//------------------------------------------------------------------------------------
void Dataflash_GetBlock(u8 BuferNo, u16 Addr, u16 Count, u8 *BuferPtr )
{
  Dataflash_Select();
  
  if ( BuferNo == 1 )	Dataflash_SPI_RW(Buf1Read);     // ������ 1
  else	if ( BuferNo == 2 ) Dataflash_SPI_RW(Buf2Read); // ������ 2
  else
    {
      Dataflash_Deselect(); // �������� � ������� �������
      return; 
    } 
  
  Dataflash_SPI_RW(0x00);
  Dataflash_SPI_RW((unsigned char)(Addr>>8));
  Dataflash_SPI_RW((unsigned char)(Addr));
  Dataflash_SPI_RW(0x00);
  
  for(u16 i=0; i<Count; i++)
    {
      *(BuferPtr) = Dataflash_SPI_RW(0x00);
      BuferPtr++;
    }
  
  Dataflash_Deselect();
}
//------------------------------------------------------------------------------------
//  ���������� ���� � SRAM-����� dataflash
//	���������: BuferNo   -> ����� ������ (1 ��� 2)
//                  Addr       -> ����� � ������, � �������� �������� ������
//                  Data       -> ����, ������� ����� ��������
//------------------------------------------------------------------------------------
void Dataflash_WriteByte(u8 BuferNo, u16 Addr, u8 Data )
{
  Dataflash_Select();
  
  if ( BuferNo == 1 )	 Dataflash_SPI_RW(Buf1Write);        // ������ 1
  else	if ( BuferNo == 2 )  Dataflash_SPI_RW(Buf2Write);   // ������ 2
  else
    {
      Dataflash_Deselect();    // �������� � ������� �������
      return; 
    }
  
  Dataflash_SPI_RW(0x00);
  Dataflash_SPI_RW((unsigned char)(Addr>>8));
  Dataflash_SPI_RW((unsigned char)(Addr));
  Dataflash_SPI_RW(Data);
  
  Dataflash_Deselect();
}
//------------------------------------------------------------------------------------
//  ���������� ���� ������ � SRAM-����� dataflash
//	���������: BuferNo   -> ����� ������ (1 ��� 2)
//                  Addr       -> ����� � ������, � �������� �������� ������
//                  Count      -> ���������� ����, ������� ���������� ��������
//  		   *BuferPtr -> �����, ������ �� �������� ����� ��������
//------------------------------------------------------------------------------------
void Dataflash_WriteBlock(u8 BuferNo, u16 Addr, u16 Count, u8 *BuferPtr )
{
  Dataflash_Select();
  
  if ( BuferNo == 1 )	 Dataflash_SPI_RW(Buf1Write);       // ������ 1
  else	if ( BuferNo == 2 )  Dataflash_SPI_RW(Buf2Write);   // ������ 2
  else
    { 
      Dataflash_Deselect(); // �������� � ������� �������
      return; 
    }
  
  Dataflash_SPI_RW(0x00);
  Dataflash_SPI_RW((unsigned char)(Addr>>8));
  Dataflash_SPI_RW((unsigned char)(Addr));
  for( u16 i=0; i<Count; i++)
    {
      Dataflash_SPI_RW(*(BuferPtr));
      BuferPtr++;
    }
  
  Dataflash_Deselect();
}
//------------------------------------------------------------------------------------
//  ��������� ���� ������ ��������������� �� Flash - ������
//	���������: PageAdr    -> ����� ��������, � ������� �������� ������
//                  Addr       -> ����� � ��������, � �������� �������� ������
//                  Count      -> ���������� ����, ������� ���������� ���������
//  		   *BuferPtr -> ����� ������� � ������� �������� ������
//------------------------------------------------------------------------------------
void Dataflash_FlashRead(u16 PageAdr, u16 Addr, u16 Count, u8 *BuferPtr )
{
  Dataflash_Select();

   Dataflash_SPI_RW(ContArrayRead);
   Dataflash_SPI_RW((unsigned char)(PageAdr >> (16 - Dataflash_Info.Page_Bit)));
   Dataflash_SPI_RW((unsigned char)((PageAdr << (Dataflash_Info.Page_Bit - 8))+ (Addr>>8)));
   Dataflash_SPI_RW((unsigned char)(Addr));
   Dataflash_SPI_RW(0x00);
   Dataflash_SPI_RW(0x00);
   Dataflash_SPI_RW(0x00);
   Dataflash_SPI_RW(0x00);

   for(u16 i=0; i < Count; i++ )
   {
      *(BuferPtr) = Dataflash_SPI_RW(0x00);
      BuferPtr++;
   }

   Dataflash_Deselect();
}
//------------------------------------------------------------------------------------
//  ����������� �� ����������
//	���������: Page�md  -> �������
//                  PageAdr  -> ����� ��������
//------------------------------------------------------------------------------------
void Dataflash_PageFunc(u8 PageCmd, u16 PageAdr )
{
   Dataflash_Select();

   Dataflash_SPI_RW(PageCmd);
   Dataflash_SPI_RW((u8)(PageAdr >> (16 - Dataflash_Info.Page_Bit)));
   Dataflash_SPI_RW((u8)(PageAdr << (Dataflash_Info.Page_Bit - 8)));
   Dataflash_SPI_RW(0x00);

   Dataflash_Deselect();

   while(!( Dataflash_GetStatus() & DF_STATUS_READY )); // ������� ���������� ��������
}
//------------------------------------------------------------------------------
//  �������� �������� � ���������� �������� ����
//------------------------------------------------------------------------------
static u8 Dataflash_SPI_RW(u8 TX)
{
  while(SPI_I2S_GetFlagStatus(Dataflash_SPI, SPI_I2S_FLAG_TXE) == RESET) {}  // ������� ������������ ������ ��������  
  SPI_I2S_SendData(Dataflash_SPI, TX);                                       // ���������� ����  
  while(SPI_I2S_GetFlagStatus(Dataflash_SPI, SPI_I2S_FLAG_RXNE) == RESET) {} // �������� ������ �����  
  return SPI_I2S_ReceiveData(Dataflash_SPI);                                 // ���������� ���������� ����
}	
/*****************************************************************************/
/*****************************************************************************/
/*             ���������� ��� �������� ������ � ������ ����������            */
/*****************************************************************************/
/*****************************************************************************/
#ifdef DATAFLASH_LINEAR_SPACE
static u16 df_linear_page_in_buffer1 = 0xffff;
extern u16 df_linear_page;
extern u16 df_linear_address;
//------------------------------------------------------------------------------------
//  ��������� ��������� �� ������ ������
//------------------------------------------------------------------------------------
void Dataflash_Linear_GoToZero()
{
  df_linear_page = 0;
  df_linear_address = 0;
}
//------------------------------------------------------------------------------------
//  ��� ������� ���������� ���������� ���� �� ����� dataflash.
//  ��������� ������� �� ���������� �������� �� ����������� ������� ������.
//  ��������� ������������ ������ ����������� ����� ��������.
//------------------------------------------------------------------------------------
u32 Dataflash_Linear_SpaceToEnd()
{
  return (u32)Dataflash_Info.Page_Size*(Dataflash_Info.Pages - df_linear_page) - (u32)df_linear_address;
}
//------------------------------------------------------------------------------------
//  ������� ������
//	���������: count      -> ���������� ����, ������� ���������� ���������
//  		   *bufer    -> ����� ������� � ������� �������� ������
//------------------------------------------------------------------------------------
void Dataflash_Linear_Read(u8 *bufer, u16 count)
{
  Dataflash_FlashRead( df_linear_page, df_linear_address, count, bufer );
  df_linear_address += count;
  
  while( df_linear_address >= Dataflash_Info.Page_Size )
    {
      df_linear_address -= Dataflash_Info.Page_Size;
      df_linear_page++;
    }
}
//------------------------------------------------------------------------------------
//  �������� ������
//	���������: count     -> ���������� ����, ������� ���������� ��������
//  		   *bufer    -> ����� ������� � ������� ��������� �������� ������
//------------------------------------------------------------------------------------
void Dataflash_Linear_UnsafeWrite(u8 *bufer, u16 count)
{
  u16 page_space_left = Dataflash_Info.Page_Size - df_linear_address;

   // ��������� ������ � ������, ���� ��� ��� �� ���.
  if ( df_linear_page != df_linear_page_in_buffer1 )
    {
      Dataflash_PageFunc(DF_FLASH_TO_BUF1, df_linear_page);
      df_linear_page_in_buffer1 = df_linear_page;
    }
  
  while (count)
    {
      page_space_left = Dataflash_Info.Page_Size - df_linear_address;

      if ( count > page_space_left )   // ����� �������� ������ ������ ��� �������� ����� �� �������� ������.
      {
        // ���������� ��� ������� �� ����� � �������
        Dataflash_WriteBlock(1, df_linear_address, page_space_left, bufer);
        count -= page_space_left;
        bufer += page_space_left;

        // ���������� ������ �� ����
        Dataflash_PageFunc(DF_BUF1_TO_FLASH_WITH_ERASE, df_linear_page);
        df_linear_address = 0;
        df_linear_page++;

        // ��������� ��������� ��������
        Dataflash_PageFunc(DF_FLASH_TO_BUF1, df_linear_page);
        df_linear_page_in_buffer1 = df_linear_page;
     }
     else
       {
         Dataflash_WriteBlock(1, df_linear_address, count, bufer);
         df_linear_address += count;
         count = 0;
       }
    }
}
//------------------------------------------------------------------------------------
//  ��������� ����� ������ � ���������� ������.
//------------------------------------------------------------------------------------
void Dataflash_Linear_Finalize()
{
  Dataflash_PageFunc(DF_BUF1_TO_FLASH_WITH_ERASE, df_linear_page);
}
//------------------------------------------------------------------------------------
//  ����������� ��������� ���������� ������
//	���������: displacement -> ��������
//------------------------------------------------------------------------------------
void Dataflash_Linear_SeekForward(u32 displacement)
{
  df_linear_page += (displacement / Dataflash_Info.Page_Size);
  df_linear_address += (displacement % Dataflash_Info.Page_Size);
}
//------------------------------------------------------------------------------------
//  ����������� ��������� ���������� �����
//	���������: displacement -> ��������
//------------------------------------------------------------------------------------
void Dataflash_Linear_SeekBackward(u32 displacement)
{
  df_linear_page -= (displacement / Dataflash_Info.Page_Size);
  df_linear_address -= (displacement % Dataflash_Info.Page_Size);
}
//------------------------------------------------------------------------------------
//  ����������� ��������� � �������� �����
//------------------------------------------------------------------------------------
void Dataflash_Linear_GoTo(u32 location)
{
  df_linear_page = (location / Dataflash_Info.Page_Size);
  df_linear_address = (location % Dataflash_Info.Page_Size);
}
//------------------------------------------------------------------------------------
#else
#warning "�������� ���������� Dataflash ���������!"
#endif /* DATAFLASH_LINEAR_SPACE */
//------------------------------------------------------------------------------------