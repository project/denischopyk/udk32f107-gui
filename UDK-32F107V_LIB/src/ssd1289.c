//-----------------------------------------------------------------------------
//              ������� ��� 320�240 �� ������ ����������� SSD1289
//-----------------------------------------------------------------------------
#include "ssd1289.h"
#include "fonts.h"



typedef union
{
  u16 U16;
  u8 U8[2];
}ColorTypeDef;

//                    ����������� ������� ������ � ���������
//-----------------------------------------------------------------------------

#define LCD_Backlight_OFF()   GPIOD->BRR  |=  GPIO_BRR_BR14;      // ���������� ��������� ���
#define LCD_Backlight_ON()    GPIOD->BSRR  |=  GPIO_BSRR_BS14;     // ��������� ��������� ���



//-----------------------------------------------------------------------------
//                          ���������� �������
//-----------------------------------------------------------------------------

#define LCD_Set_RS    GPIOC->BSRR |=  GPIO_BSRR_BS8;  // ��������� ����� RS � 1
#define LCD_Clr_RS    GPIOC->BRR  |=  GPIO_BRR_BR8;   // ����� ����� RS � 0
#define LCD_Set_RD    GPIOD->BSRR |=  GPIO_BSRR_BS15;   // ��������� ����� RD � 1
#define LCD_Clr_RD    GPIOD->BRR  |=  GPIO_BRR_BR15; // ����� ����� RD � 0   
#define LCD_Set_WR    GPIOC->BSRR |=  GPIO_BSRR_BS6;   // ��������� ����� WR � 1
#define LCD_Clr_WR    GPIOC->BRR  |=  GPIO_BRR_BR6;  // ����� ����� WR � 0
#define LCD_Set_CS    GPIOC->BSRR |=  GPIO_BSRR_BS9;   // ��������� ����� CS � 1
#define LCD_Clr_CS    GPIOC->BRR  |=  GPIO_BRR_BR9; // ����� ����� CS � 0


static  void LCD_Write_Data(u16 Data);               // �������� ������ ���
static  u16  LCD_Read_Data();                        // ������ ������ �� ���

//-----------------------------------------------------------------------------
//                          ������������� ���
//-----------------------------------------------------------------------------
void LCD_Init()
{   
//��������� ������������ GPIOD, GPIOC, GPIOE  
 RCC->APB2ENR |= (RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPEEN);    
 
 // BLACKLIGHT - PD.14, 
GPIOD->CRH   &= ~GPIO_CRH_MODE14;      //�������� ������� MODE
GPIOD->CRH   &= ~GPIO_CRH_CNF14;       //�������� ������� CNF
GPIOD->CRH   |=  GPIO_CRH_MODE14_1;    //�����, 2MHz
GPIOD->CRH   |=  GPIO_CRH_CNF14_0;     //������ ����������, �������� ����
 // RD - PD.15
GPIOD->CRH   &= ~GPIO_CRH_MODE15;      //�������� ������� MODE
GPIOD->CRH   &= ~GPIO_CRH_CNF15;       //�������� ������� CNF
GPIOD->CRH   |=  GPIO_CRH_MODE15;      //�����, 50MHz
GPIOD->CRH   &= ~GPIO_CRH_CNF15;       //������ ����������, �����������  
// RS -PC.8, CS - PC.9
GPIOC->CRH   &= ~GPIO_CRH_MODE8;       //�������� ������� MODE
GPIOC->CRH   &= ~GPIO_CRH_CNF8;        //�������� ������� CNF
GPIOC->CRH   |=  GPIO_CRH_MODE8;       //�����, 50MHz
GPIOC->CRH   &= ~GPIO_CRH_CNF8;        //������ ����������, ����������� 

GPIOC->CRH   &= ~GPIO_CRH_MODE9;       //�������� ������� MODE
GPIOC->CRH   &= ~GPIO_CRH_CNF9;        //�������� ������� CNF
GPIOC->CRH   |=  GPIO_CRH_MODE9;       //�����, 50MHz
GPIOC->CRH   &= ~GPIO_CRH_CNF9;        //������ ����������, ����������� 
// WR -PC.6
GPIOC->CRL   &= ~GPIO_CRL_MODE6;       //�������� ������� MODE
GPIOC->CRL   &= ~GPIO_CRL_CNF6;        //�������� ������� CNF
GPIOC->CRL   |=  GPIO_CRL_MODE6;       //�����, 50MHz
GPIOC->CRL   &= ~GPIO_CRL_CNF6;        //������ ����������, �����������  
// DATA - PE
GPIOE->CRL   &= ~GPIO_CRL_MODE;      //�������� ������� MODE
GPIOE->CRL   &= ~GPIO_CRL_CNF;       //�������� ������� CNF
GPIOE->CRL   |=  GPIO_CRL_MODE;      //�����, 50MHz
GPIOE->CRL   &= ~GPIO_CRL_CNF;       //������ ����������, �����������  

GPIOE->CRH   &= ~GPIO_CRH_MODE;      //�������� ������� MODE
GPIOE->CRH   &= ~GPIO_CRH_CNF;       //�������� ������� CNF
GPIOE->CRH   |=  GPIO_CRH_MODE;      //�����, 50MHz
GPIOE->CRH   &= ~GPIO_CRH_CNF;       //������ ����������, �����������  

   
  LCD_Set_RD;
  LCD_Set_WR;
  LCD_Set_CS;
  LCD_Set_RS;
  LCD_Backlight_ON();
   
  LCD_Write_REG(0x0000,0x0001);    // ������ ����������� ����������
  LCD_Write_REG(0x0001,0x693F);    // ���������� ��������� ������
  //LCD_Write_REG(0x0001,0x6B3F);    // ��������� �� ��� X
  //LCD_Write_REG(0x0001,0x293F);    // ��������� �� ��� Y
  LCD_Write_REG(0x0002,0x0600);    // 
  LCD_Write_REG(0x0003,0xA8A4);    // 
  
  LCD_Write_REG(0x0005,0x0000);    //
  LCD_Write_REG(0x0006,0x0000);    //
  LCD_Write_REG(0x0007,0x0233);    // 
  
  LCD_Write_REG(0x000B,0x0000);    //
  LCD_Write_REG(0x000C,0x0000);    // RGB interface setting
  LCD_Write_REG(0x000D,0x080C);    //    
  LCD_Write_REG(0x000E,0x2B00);    //  
  LCD_Write_REG(0x000F,0x0000);    // Gate scan position
  LCD_Write_REG(0x0010,0x0000);    //
  //LCD_Write_REG(0x0011,0x4070);    //  262000 ������
  LCD_Write_REG(0x0011,0x6070);    //  65000 ������
  
  LCD_Write_REG(0x0015,0x00D0);    //
  LCD_Write_REG(0x0016,0xEF1C);    //
  LCD_Write_REG(0x0017,0x0003);    //
  
  LCD_Write_REG(0x001E,0x00B0);    //   
  
  LCD_Write_REG(0x0022,0x0022);    //
  LCD_Write_REG(0x0023,0x0000);    //
  LCD_Write_REG(0x0024,0x0000);    //
  LCD_Write_REG(0x0025,0x8000);    //
  
  LCD_Write_REG(0x0030,0x0707);    //
  LCD_Write_REG(0x0031,0x0204);    //
  LCD_Write_REG(0x0032,0x0204);    //
  LCD_Write_REG(0x0033,0x0502);    //
  LCD_Write_REG(0x0034,0x0507);    //
  LCD_Write_REG(0x0035,0x0204);    //
  LCD_Write_REG(0x0036,0x0204);    //
  LCD_Write_REG(0x0037,0x0502);    //
  LCD_Write_REG(0x003A,0x0302);    //
  LCD_Write_REG(0x003B,0x0302);    //
  
  LCD_Write_REG(0x0041,0x0000);    //
  LCD_Write_REG(0x0042,0x0000);    //
  
  LCD_Write_REG(0x0044,0xEF00);    //
  LCD_Write_REG(0x0045,0x0000);    //
  LCD_Write_REG(0x0046,0x013F);    //
  
  LCD_Write_REG(0x0048,0x0000);    //
  LCD_Write_REG(0x0049,0x013F);    //
  LCD_Write_REG(0x004A,0x0000);    //
  LCD_Write_REG(0x004B,0x0000);    //
  
  LCD_Write_REG(0x004E,0x0000);    // X=0
  LCD_Write_REG(0x004F,0x0000);    // Y=0 
}

//-----------------------------------------------------------------------------
//                      ������ ������ � ������� ���
//-----------------------------------------------------------------------------
void LCD_Write_REG(u16 Adr, u16 Data)
{
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Clr_RS;                                          /*   RS\   */
  
  GPIOE->ODR  = Adr;        // �������� ������ �������� ���
    
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_RS;                                          /*   RS/   */
  
  GPIOE->ODR  =  Data;                // �������� ������ � ������� ���
  
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}
//-----------------------------------------------------------------------------
//                     �������� �������� ���
//-----------------------------------------------------------------------------
void LCD_Write_Command(u8 Comm)
{
  GPIOE->ODR  =  Comm;                // �������� �������� � ���� ���                
   
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Clr_RS;                                          /*   RS\   */
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}
//-----------------------------------------------------------------------------
//                     �������� ������ ���
//-----------------------------------------------------------------------------
static void LCD_Write_Data(u16 Data) 
{
  GPIOE->ODR  =  Data;                // �������� ������ � ���� ���
   
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Set_RS;                                          /*   RS/   */
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}
//-----------------------------------------------------------------------------
//                      ������ ������ �� ���
//-----------------------------------------------------------------------------
static u16 LCD_Read_Data() 
{
  unsigned int Data; 
  GPIOE->CRH   &= ~GPIO_CRH_MODE;      //�������� ������� MODE
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //�������� ������� CNF
  GPIOE->CRH   |=  0x44444444;     //���������� ����, ������ ���������   
  GPIOE->CRL   &= ~GPIO_CRL_MODE;      //�������� ������� MODE
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //�������� ������� CNF
  GPIOE->CRL   |=  0x44444444;     //���������� ����, ������ ��������� 
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Set_RS;                                          /*   RS/   */
  LCD_Clr_RD;                                          /*   RD\   */
  
  Data = GPIOE->IDR;        // ������ ������ �� ���
   
  LCD_Set_RD;                                          /*   RD/   */
  LCD_Set_CS;                                          /*   CS/   */
  
  GPIOE->CRL   &= ~GPIO_CRL_MODE;      //�������� ������� MODE
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //�������� ������� CNF
  GPIOE->CRL   |=  GPIO_CRL_MODE;      //�����, 50MHz
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //������ ����������, �����������  

  GPIOE->CRH   &= ~GPIO_CRH_MODE;      //�������� ������� MODE
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //�������� ������� CNF
  GPIOE->CRH   |=  GPIO_CRH_MODE;      //�����, 50MHz
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //������ ����������, �����������
  return(Data);
}
//-----------------------------------------------------------------------------
//                      ������ ������ �� �������� ���
// Adr - ����� ��������
//-----------------------------------------------------------------------------
u16 LCD_Read_REG(u16 Adr)
{
  LCD_Write_Command(Adr);                   // ������ ������ �������� � ���             
  return(LCD_Read_Data());                  // ���������� ��������� ������ �� �������� ���
}
//-----------------------------------------------------------------------------
//                      ��������� ������� ���
// x,y - ���������� �������
//-----------------------------------------------------------------------------
void LCD_SetCursor(u16 x,u16 y)
{ 
  LCD_Write_REG(0x004E,x);                   
  LCD_Write_REG(0x004F,y);                   
  LCD_Write_Command(0x22);                 
}
//-----------------------------------------------------------------------------
//                     ���������� ����� �� ���
// x,y - ���������� �����
// Color - ���� �����
//-----------------------------------------------------------------------------
void LCD_SetPoint(u16 x,u16 y,u16 Color)
{
  LCD_SetCursor(y,x);

  LCD_Clr_CS;                               /*   CS\   */
  LCD_Write_Command(0x22);
  LCD_Set_RS;                               /*   RS/   */
  LCD_Write_Data(Color);
  LCD_Clr_WR;                               /*   WR\   */
  LCD_Set_WR;                               /*   WR/   */
  LCD_Set_CS;                               /*   CS/   */
}
//-----------------------------------------------------------------------------
//                       ������� ������ ����� ������
//-----------------------------------------------------------------------------
void LCD_FillScreen(u16 Color)
{
  LCD_SetArea(0, 0, 239, 319);	
  
  LCD_Clr_CS;                             /*   CS\   */
  
  LCD_Set_RS;                             /*   RS/   */
  
  GPIOE->ODR  = Color;  // �������� ������ � ���� ���
  
  for(int i=0; i < 76800; i++)
  {    
    LCD_Clr_WR;                           /*   WR\   */
    LCD_Set_WR;                           /*   WR/   */
  }
  LCD_Set_CS;                             /*   CS/   */
}
//-----------------------------------------------------------------------------
//                       ����� ���� ��� ���������
//-----------------------------------------------------------------------------
void LCD_SetArea(u16 x1, u16 y1, u16 x2, u16 y2)
{
  LCD_Write_Command(0x44); LCD_Write_Data((x2 << 8) | x1);    // Source RAM address window 
  LCD_Write_Command(0x45); LCD_Write_Data(y1);    // Gate RAM address window 
  LCD_Write_Command(0x46); LCD_Write_Data(y2);    // Gate RAM address window 
  LCD_SetCursor(x1, y1);
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� ������� ������� 5�7 ��������
// x,y - ��������� ���������� ������� ���������� �������
// c - ��� ������
// t_color - ���� �������
// b_color - ���� ����
// rot - ������� ������� : ����������������, ���� �� ����� 0
// zoom - ���������, ������������� ����� � ����� ����� ���, ������� � 1
//-----------------------------------------------------------------------------
void LCD_WriteChar5x7(u16 x, u16 y, char c, u16 t_color, u16 b_color, u8 rot, u8 zoom )
{
  unsigned char h,ch,p,mask,z,z1;

  if (rot != 0) LCD_SetArea(x, y, x+(6*zoom)-1, y+(8*zoom)-1);	
  else   	LCD_SetArea(y, x, y+(8*zoom)-1, x+(6*zoom)-1);

  for (h=0; h<6; h++) 
    {
      if(h < 5)
        {
          if(c < 129) ch=font_5x7[ c-32 ][h];
          else        ch=font_5x7[ c-32-63 ][h];
          
          if (rot != 0)	
            {
              LCD_Write_REG(0x0011,0x6078);
              LCD_Write_Command(0x22);
            }
        }
      else ch = 0;
      
      z1 = zoom;
      while(z1 != 0)
        {
          if (rot != 0) mask=0x01;
          else mask=0x80;
          
          for (p=0; p<8; p++)  
            {
              z = zoom;
              while(z!=0)
                {
                  if (ch&mask) LCD_Write_Data(t_color);
                  else  LCD_Write_Data(b_color);
                  
                  z--;
                }
              if (rot != 0) mask=mask<<1;
              else mask=mask>>1;
            }
          z1--;
	}
     }
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� ������� ������� 8�16 ��������
// x,y - ��������� ���������� ������� ���������� �������
// c - ��� ������
// t_color - ���� �������
// b_color - ���� ����
//-----------------------------------------------------------------------------
void LCD_WriteChar_8x16(u16 x, u16 y, char c, u16 t_color, u16 b_color)
{
  unsigned char tmp_char=0;
  
  for (u8 i=0;i<16;i++) 
  {
    tmp_char=font_8x16[((c-0x20)*16)+i];
    for (u8 j=0;j<8;j++)
      {
        if ( (tmp_char >> 7-j) & 0x01 == 0x01) LCD_SetPoint(x+j,y+i,t_color); // ���� �������
        else LCD_SetPoint(x+j,y+i,b_color); // ���� ����        
      } 
  }
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� ������ ������� 8x16 ��������
// x,y - ��������� ���������� ������� ���������� ������
// *text - ��� �����
// t_color - ���� �������
// b_color - ���� ����
//-----------------------------------------------------------------------------
void LCD_WriteString_8x16(u16 x, u16 y, char *text, u16 charColor, u16 bkColor)
{
  for (u8 i=0; *text; i++) LCD_WriteChar_8x16((x+8*i), y, *text++, charColor, bkColor);
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� ������ ������� 5�7 ��������
// x,y - ��������� ���������� ������� ���������� ������
// *text - ��� �����
// t_color - ���� �������
// b_color - ���� ����
// rot - ������� ������ : ����������������, ���� �� ����� 0
// zoom - ���������, ������������� ����� � ����� ����� ���, ������� � 1
//-----------------------------------------------------------------------------
void LCD_WriteString_5x7(u16 x, u16 y, char *text, u16 charColor, u16 b_color, u8 rot, u8 zoom)
{
  for (u8 i=0; *text; i++) LCD_WriteChar5x7(x+(i*6*zoom),y,*text++,charColor, b_color, rot, zoom);
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� �����
// (x1,y1) - ���������� ������ �����
// (x2,y2) - ���������� ����� �����
// color - ���� �����
//-----------------------------------------------------------------------------
void LCD_Draw_Line(u16 x1, u16 y1, u16 x2, u16 y2, u16 color)
{
  u16 x, y, dx, dy;
  if(y1==y2)
    {
      if(x1<=x2) x=x1;
      else
        {
          x=x2;
	  x2=x1;
	}
      
      while(x <= x2)
        {
          LCD_SetPoint(x,y1,color);
          x++;
  	}
      return;
    }
  
  else if(y1>y2) dy=y1-y2;
       else dy=y2-y1;
       
  if(x1==x2)
    {
      if(y1<=y2) y=y1;
      else
        {
          y=y2;
	  y2=y1;
	}
      
      while(y <= y2)
        {
          LCD_SetPoint(x1,y,color);
          y++;
  	}
      return;
    }
  
  else if(x1 > x2)
    {
      dx=x1-x2;
      x = x2;
      x2 = x1;
      y = y2;
      y2 = y1;
    }
       else
         {
           dx=x2-x1;
           x = x1;
           y = y1;
 	 }
  if(dx == dy)
    {
      while(x <= x2)
        {
          x++;
          if(y>y2) y--;
          
          else y++;
          LCD_SetPoint(x,y,color);
  	}
    } 
  else
    {
      LCD_SetPoint(x, y, color);
      if(y < y2)
        {
          if(dx > dy)
            {
              s16 p = dy * 2 - dx;
              s16 twoDy = 2 * dy;
              s16 twoDyMinusDx = 2 * (dy - dx);
              while(x < x2)
                {
                  x++;
                  if(p < 0) p += twoDy;
                  else
                    {
                      y++;
                      p += twoDyMinusDx;
     		    }
                  LCD_SetPoint(x, y,color);
    		}
   	    }
          else
            {
              s16 p = dx * 2 - dy;
              s16 twoDx = 2 * dx;
              s16 twoDxMinusDy = 2 * (dx - dy);
              while(y < y2)
                {
                  y++;
                  if(p < 0) p += twoDx;
                  else
                    {
                      x++;
      		      p+= twoDxMinusDy;
     		    }
                  LCD_SetPoint(x, y, color);
    		}
   	    }
  	}
      else
        {
          if(dx > dy)
            {
              s16 p = dy * 2 - dx;
              s16 twoDy = 2 * dy;
              s16 twoDyMinusDx = 2 * (dy - dx);
              while(x < x2)
                {
                  x++;
                  if(p < 0) p += twoDy;
                  else
                    {
                      y--;
                      p += twoDyMinusDx;
                    }
                  LCD_SetPoint(x, y,color);
    		}
   	     }
          else
            {
              s16 p = dx * 2 - dy;
              s16 twoDx = 2 * dx;
              s16 twoDxMinusDy = 2 * (dx - dy);
              while(y2 < y)
                {
                  y--;
                  if(p < 0) p += twoDx;
                  else
                    {
                      x++;
                      p+= twoDxMinusDy;
                    }
                  LCD_SetPoint(x, y,color);
    		}
   	     }
  	 }
      }
}
//-----------------------------------------------------------------------------
//                       ����� �� ��� ����������
// (cx,cy) - ���������� ������ ���������� 
// color - ���� ����������
// fill - ����������� ����, ���� !== 0
//-----------------------------------------------------------------------------
void LCD_Draw_Circle(u16 cx,u16 cy,u16 r,u16 color,u8 fill)
{
  u16 x=0,y=r;
  s16 delta=3-(r<<1),tmp;
  
  while(y>x)
    {
      if(fill)
        {
          LCD_Draw_Line(cx+x,cy+y,cx-x,cy+y,color);
          LCD_Draw_Line(cx+x,cy-y,cx-x,cy-y,color);
          LCD_Draw_Line(cx+y,cy+x,cx-y,cy+x,color);
          LCD_Draw_Line(cx+y,cy-x,cx-y,cy-x,color);
        }
      else
        {
          LCD_SetPoint(cx+x,cy+y,color);
          LCD_SetPoint(cx-x,cy+y,color);
          LCD_SetPoint(cx+x,cy-y,color);
          LCD_SetPoint(cx-x,cy-y,color);
          LCD_SetPoint(cx+y,cy+x,color);
          LCD_SetPoint(cx-y,cy+x,color);
          LCD_SetPoint(cx+y,cy-x,color);
          LCD_SetPoint(cx-y,cy-x,color);
        }
      x++;
      if(delta>=0)
        {
          y--;
          tmp=(x<<2);
          tmp-=(y<<2);
          delta+=(tmp+10);
        }
      else delta+=((x<<2)+6);		
     }
}

//-----------------------------------------------------------------------------
//                       ����� �� ��� ����������
// (Xc,Yc) - ���������� ������ ���������� 
// color - ���� ����������

//-----------------------------------------------------------------------------

void Lcd_Circle(u16  Xc, u16 Yc, u16 r, u16 color) {
  s16 x,y,d;
  d = 3 - (r*2);
  x = 0;
  y = r;
  while(x<=y) {
   LCD_SetPoint(Xc+x,Yc+y, color);
   LCD_SetPoint(Xc+x,Yc-y, color);
   
   LCD_SetPoint(Xc-x,Yc+y, color);
   LCD_SetPoint(Xc-x,Yc-y, color);
   LCD_SetPoint(Xc+y,Yc+x, color);
   LCD_SetPoint(Xc+y,Yc-x, color);
   LCD_SetPoint(Xc-y,Yc+x, color);
   LCD_SetPoint(Xc-y,Yc-x, color);

   if(d<0) {
     d = d + (x*4) + 6;
   }
   else {
     d = d + ((x-y)*4) + 10;
     y--;
   }
   x++;
  }
}

//-----------------------------------------------------------------------------
//                     ����� �� ��� ��������������
//  (x1,y1) - ���������� ������ ������� ���� ��������������
//  (x2,y2) - ���������� ������� �������� ���� ��������������
//  color - ���� ��������������
//  fill - ����������� �������������, ���� !== 0
//-----------------------------------------------------------------------------
void LCD_Draw_Rectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color,u8 fill)
{
  if(fill)
    {
      u16 i;
      if(x1>x2)
        {
          i=x2;
          x2=x1;
        }
      else i=x1;
      for(;i<=x2;i++) LCD_Draw_Line(i,y1,i,y2,color);
      return;
    }
  LCD_Draw_Line(x1,y1,x1,y2,color);
  LCD_Draw_Line(x1,y2,x2,y2,color);
  LCD_Draw_Line(x2,y2,x2,y1,color);
  LCD_Draw_Line(x2,y1,x1,y1,color);
}
//-----------------------------------------------------------------------------
//                    ����� �� ��� ������� �� �������
// (x0,y0) - ���������� ������ ������� ���� �������
// *str - ������ �������
//-----------------------------------------------------------------------------
void LCD_Draw_Picture(u16 x0, u16 y0, const unsigned char *str)
{
  u32 temp, i;
  u16 y1, imageWidth, imageHeight;
  ColorTypeDef color;
  
  color.U8[1] =*(unsigned short *)(&str[ 0]);
  color.U8[0]=*(unsigned short *)(&str[ 1]);
  imageWidth = color.U16;
  
  
  color.U8[1] =*(unsigned short *)(&str[ 2]);
  color.U8[0]=*(unsigned short *)(&str[ 3]);
  imageHeight = color.U16;
  y1 = imageHeight + y0;
  
  temp = 2;
  while (y0<=y1-1) 
    {
      LCD_SetCursor(x0, y0);
      LCD_Clr_CS;              /*   CS\   */
      LCD_Clr_RS;              /*   RS\   */
      LCD_Set_RD;              /*   RD/   */
      LCD_Write_Data(0x0022);
      LCD_Clr_WR;              /*   WR\   */
      LCD_Set_WR;              /*   WR/   */
      LCD_Set_RS;              /*   RS/   */
      for(i=0;i<=imageWidth-1;i++) 
        {  //Write all pixels to ram, and then update display (fast)
          color.U8[1] =*(unsigned short *)(&str[ 2 * temp]);
	  color.U8[0]=*(unsigned short *)(&str[ 2 * temp+1]);
          LCD_Write_Data(color.U16);
          LCD_Clr_WR;
          LCD_Set_WR;
          temp++;
        } 
      y0++;
      LCD_Set_CS;     /*   CS/   */
    }
}

//-----------------------------------------------------------------------------
//                    ����� �� ��� BMP �� ������� 16 bit
// (x0,y0) - ���������� ������ ������� ���� �������
// *str - ������ �������
//-----------------------------------------------------------------------------
void LCD_Draw_BMP(u16 x0, u16 y0, const unsigned int *str, u8 clck)
{
  u32 temp, i;
  u16 y1, imageWidth, imageHeight;
  u16 color;
  imageWidth = *(unsigned int *)(&str[0]);
  imageHeight = *(unsigned int *)(&str[ 1]);
  y1 = imageHeight + y0;
  
  if (!clck){
  temp = 2;
  while (y0<=y1-1) 
    {
      for(i=0;i<=imageWidth-1;i++) 
        {  
          color =*(unsigned int *)(&str[temp]);
	  LCD_SetPoint(x0+i,y1,color);
          temp++;
        } 
      y1--;
     }
  } else {
   temp = (imageWidth*imageHeight)+2;
  while (y0<=y1-1) 
    {
      for(i=0;i<=imageWidth-1;i++) 
        {  
          color =*(unsigned int *)(&str[temp]);
	  LCD_SetPoint(x0+i,y1,color);
          temp++;
        } 
      y1--;
     }  
}
}
//-----------------------------------------------------------------------------