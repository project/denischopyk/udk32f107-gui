//-------------------------------------------------------------------------------------
//                   ������� �����������
//-------------------------------------------------------------------------------------
#include "leds.h"


//-------------------------------------------------------------------------------------
// ������������� �����������, � ����������� �� �������� LED :
// - USER_LED - ��������� ������������
// - FAULT_LED - ��������� ������
// - BOTH_LEDS - ��� ����������
//-------------------------------------------------------------------------------------
void LED_Init (LED_TypeDef LED)
{
    
  if ((LED == USER_LED) | (LED == BOTH_LEDS))
  {
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;     // �������� ������������ ����� ���������� ������������
 // RS user PB.10
 GPIOB->CRH   &= ~GPIO_CRH_MODE10;       //�������� ������� MODE
 GPIOB->CRH   &= ~GPIO_CRH_CNF10;        //�������� ������� CNF
 GPIOB->CRH   |=  GPIO_CRH_MODE10;       //�����, 50MHz
 GPIOB->CRH   &= ~GPIO_CRH_CNF10;        //������ ����������, ����������� 
  }
  
   if ((LED == FAULT_LED) | (LED == BOTH_LEDS))
  {
    RCC->APB2ENR |= RCC_APB2ENR_IOPDEN;    // �������� ������������ ����� ���������� ������
 
   GPIOD->CRH   &= ~GPIO_CRH_MODE12;      //�������� ������� MODE
   GPIOD->CRH   &= ~GPIO_CRH_CNF12;       //�������� ������� CNF
   GPIOD->CRH   |=  GPIO_CRH_MODE12;      //�����, 50MHz
   GPIOD->CRH   |=  GPIO_CRH_CNF12_0;     //������ ����������, �������� ����
   
  }  
}

//-------------------------------------------------------------------------------------
// ��������� �����������, � ����������� �� �������� LED :
// - USER_LED - ��������� ������������
// - FAULT_LED - ��������� ������
// - BOTH_LEDS - ��� ����������
//-------------------------------------------------------------------------------------
void LED_On (LED_TypeDef LED)
{
    if ((LED == USER_LED) | (LED == BOTH_LEDS))
  {
   GPIOB->BSRR  =  GPIO_BSRR_BS10; 
  
  }
  
  if ((LED == FAULT_LED) | (LED == BOTH_LEDS))
  {
    GPIOD->BRR  =  GPIO_BRR_BR12;
    
    
  } 
}
//-------------------------------------------------------------------------------------
// ���������� �����������, � ����������� �� �������� LED :
// - USER_LED - ��������� ������������
// - FAULT_LED - ��������� ������
// - BOTH_LEDS - ��� ����������
//-------------------------------------------------------------------------------------
void LED_Off (LED_TypeDef LED)
{
  if ((LED == USER_LED) | (LED == BOTH_LEDS))
  {
     GPIOB->BRR  =  GPIO_BRR_BR10; 
     
  }
  
  if ((LED == FAULT_LED) | (LED == BOTH_LEDS))
  {
    GPIOD->BSRR  =  GPIO_BSRR_BS12;
        
  } 
}
//-------------------------------------------------------------------------------------