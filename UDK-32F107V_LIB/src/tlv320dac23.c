#include "tlv320dac23.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
//-------------------------------------------------------------------------------------
// ������������� ������
//-------------------------------------------------------------------------------------
void TLV320_Init (void)
{
        GPIO_InitTypeDef GPIO_InitStructure;
        I2S_InitTypeDef I2S_InitStructure;
        I2C_InitTypeDef  I2C_InitStructure; 
  
        RCC_APB2PeriphClockCmd(TLV320_I2C_GPIO_CLK |         // �������� ������������ ������
                               TLV320_I2S_CK_GPIO_CLK |
                               TLV320_I2S_SD_GPIO_CLK |
                               TLV320_I2S_MCK_GPIO_CLK |
                               TLV320_I2S_WS_GPIO_CLK |
                               TLV320_I2S_MISO_GPIO_CLK |
                               RCC_APB2Periph_AFIO, ENABLE); // � AFIO
 
        RCC_APB1PeriphClockCmd(TLV320_SPI_CLK |              // �������� ������������ SPI
                               TLV320_I2C_CLK, ENABLE);      // � I2C1 
        
        /* ��������� ������� ���������� I2S */
        GPIO_InitStructure.GPIO_Pin = TLV320_I2S_CK_PIN;          // CK - 
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;         // Alternative function
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;           // push-pull 50 MHz
        GPIO_Init(TLV320_I2S_CK_GPIO_PORT, &GPIO_InitStructure);
        
        GPIO_InitStructure.GPIO_Pin = TLV320_I2S_SD_PIN;          // SD - Alternative function push-pull 50 MHz
        GPIO_Init(TLV320_I2S_SD_GPIO_PORT, &GPIO_InitStructure);
        
        GPIO_InitStructure.GPIO_Pin = TLV320_I2S_MCK_PIN;         // MCK - Alternative function
        GPIO_Init(TLV320_I2S_MCK_GPIO_PORT, &GPIO_InitStructure); // push-pull 50 MHz
        
        GPIO_InitStructure.GPIO_Pin = TLV320_I2S_WS_PIN;          // WS - Alternative function
        GPIO_Init(TLV320_I2S_WS_GPIO_PORT, &GPIO_InitStructure);  // push-pull 50 MHz
        
        GPIO_InitStructure.GPIO_Pin = TLV320_I2S_MISO_PIN;        // MISO - Input
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;     // floating
        GPIO_Init(TLV320_I2S_MISO_GPIO_PORT, &GPIO_InitStructure);
        
        GPIO_PinRemapConfig(TLV320_SPI_Remap, TLV320_SPI_PINS_Remap); // Remap SPI             
        
        /* ��������� ������� SCL � SDA */
        GPIO_InitStructure.GPIO_Pin =  TLV320_I2C_SCL_PIN | TLV320_I2C_SDA_PIN; // ������ PB8 � PB9 -
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;                       // alternative function
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;                         // open drain
        GPIO_Init(TLV320_I2C_GPIO_PORT, &GPIO_InitStructure);                   // ������������� ������� ������        
        GPIO_PinRemapConfig(TLV320_I2C_Remap, TLV320_I2C_PINS_Remap);           // Remap I2C     

        /* ��������� ������ I2S */
        SPI_I2S_DeInit(TLV320_SPI);                                    // ��������� SPI
        I2S_InitStructure.I2S_Standard = I2S_Standard_Phillips;        // 
        I2S_InitStructure.I2S_DataFormat = I2S_DataFormat_16b;         //
        I2S_InitStructure.I2S_MCLKOutput = I2S_MCLKOutput_Disable;     //
        I2S_InitStructure.I2S_AudioFreq = I2S_AudioFreq_48k;           //
        I2S_InitStructure.I2S_CPOL = I2S_CPOL_Low;                     //
        I2S_InitStructure.I2S_Mode = I2S_Mode_MasterTx;                //
        I2S_Init(TLV320_SPI, &I2S_InitStructure);                      // I2S configuration 
        I2S_Cmd(TLV320_SPI, ENABLE);                                   // Enable the I2S 
        //SPI_I2S_ITConfig(TLV320_SPI, SPI_I2S_IT_TXE, DISABLE);
                            
	/* ��������� ������ I2C */
        I2C_DeInit(TLV320_I2C);                                                   // ��������� I2C
        I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;                                // ����� I2C
        I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_16_9;                     // Duty cycle 16:9
        I2C_InitStructure.I2C_OwnAddress1 = 1;                                    // ����� ������� 1
        I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;                               // ��������� �������� ACK
        I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // ����� ACK ����� 7-���������� ������
        I2C_InitStructure.I2C_ClockSpeed = TLV320_I2C_Speed;                      // ��������� �������� ������ �� I2C
        I2C_Cmd(TLV320_I2C, ENABLE);                                              // ��������� ������ I2C      
        I2C_Init(TLV320_I2C, &I2C_InitStructure);                                 // ������������� ������ I2C
        I2C_AcknowledgeConfig(TLV320_I2C, ENABLE);                                // ��������� �������� ACK
        
	/* ��������� ��������� ������ */
	TLV320_WriteRegister(TLV320_REG_RESET, 0x00);                                     // ����� ��������� ������
	TLV320_WriteRegister(TLV320_REG_POWER, POWER_LINE);                               // Line in powered down
	TLV320_Set_Sample_Rate(44100);                                                    // ��������� �������� 44100
	TLV320_WriteRegister(TLV320_REG_AN_PATH, AN_PATH_DAC|AN_PATH_INSEL|AN_PATH_MICB); // enable DAC,input select=MIC, mic boost
	TLV320_WriteRegister(TLV320_REG_DIG_PATH, 0 << 3);                                // disable soft mute        
	TLV320_WriteRegister(TLV320_REG_DIG_FORMAT, DIG_FORMAT_MS | DIG_FORMAT_FOR1);     // master, I2S format, MSB first, left � aligned                
        TLV320_WriteRegister(TLV320_REG_LOUTVOL, TLV320_Default_Volume);                  // ��������� ������ ������
        TLV320_WriteRegister(TLV320_REG_ROUTVOL, TLV320_Default_Volume);                  // ��������� ������� ������       
	TLV320_WriteRegister(TLV320_REG_DIG_ACT, DIG_ACT_ACT);                            // ��������� ��������� ���������� TLV320AIC23     */  
}
//-------------------------------------------------------------------------------------
// ������ ������ � ������� ������
//-------------------------------------------------------------------------------------
void TLV320_WriteRegister (unsigned char reg, unsigned short value)
{
  unsigned char MSB, LSB;
  
        LSB = value & 0xFF;
        MSB = (reg << 1) | ((value >> 8) & 0x01);

        I2C_GenerateSTART(TLV320_I2C, ENABLE);                                            // �������� ������� START
        while(!I2C_CheckEvent(TLV320_I2C, I2C_EVENT_MASTER_MODE_SELECT));                 // ������� ��������� ������ Master (EV5) � ���������� ����      
	I2C_Send7bitAddress(TLV320_I2C, TLV320_I2C_ADDR << 1, I2C_Direction_Transmitter); // �������� ���. ������ ������ �� ���� I2C
        while(!I2C_CheckEvent(TLV320_I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));   // ������� ��������� ������ Master transmitter (EV6) � ���������� ���� 
        I2C_SendData(TLV320_I2C, MSB);                                                    // �������� MSB
        while(!I2C_CheckEvent(TLV320_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));            // ������� ��������� �������� MSB (EV8_2) � ���������� ����       
        I2C_SendData(TLV320_I2C, LSB);                                                    // �������� LSB
        while(!I2C_CheckEvent(TLV320_I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED));            // ������� ��������� �������� LSB (EV8_2) � ���������� ����         
        I2C_GenerateSTOP(TLV320_I2C, ENABLE);                                             // �������� ������� STOP
}
//-------------------------------------------------------------------------------------
// ��������� ��������
//-------------------------------------------------------------------------------------
int TLV320_Set_Sample_Rate(unsigned long srate)
{
	switch(srate) {
	case 8000:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR1|SRATE_SR0|SRATE_USB);		                        break;
	case 8021:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR3|SRATE_SR1|SRATE_SR0|SRATE_BOSR|SRATE_USB);	        break;
	case 32000:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR2|SRATE_SR1|SRATE_USB);			                break;
	case 44100:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR3|SRATE_BOSR|SRATE_USB);			                break;
	case 48000:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_USB);					                break;
	case 88200:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR3|SRATE_SR2|SRATE_SR1|SRATE_SR0|SRATE_BOSR|SRATE_USB);	break;
	case 96000:	TLV320_WriteRegister(TLV320_REG_SRATE, SRATE_SR2|SRATE_SR1|SRATE_SR0|SRATE_USB);			break;
	default:	return -1;
	}
	return 0;
}
