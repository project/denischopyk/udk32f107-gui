#include "graphicTypes.h"
#include "ssd1289.h"


//-----------------------------------------------------------------------------
//                    ���������  Label - ����������� �����
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������
// text - ��� �����
// rot - ������� ������: ����������������, ���� �� ����� 0
// zoom - ���������, ������������� ����� � ����� ����� ���, ������� � 1
// fType - ��� ������, ��. graphicTypes.h - FontSise 
// vis - ���������. 1 - �������, 0 - �� �������
//-----------------------------------------------------------------------------


#if LABNUM  // ���� ��������� �������� - �� ����� ����

extern Lab Label[LABNUM]; //������� ����������
// �������� ����������
void LabelCreate(u8 id, u16 x, u16 y, char *text, u8 rot, u8 zoom, FontSise fType) {
  Label[id].ID = id;
  Label[id].Left = x;
  Label[id].Top = y;
  Label[id].Caption = text;
  Label[id].Rot = rot;
  Label[id].Zoom = zoom;
  Label[id].FontType = fType; 
  LabelShow(id, 1);
}
// �������� ���������
 void LabelShow(u8 id, u8 vis) {
   if (vis) {//�������
     if (Label[id].FontType) { 
       LCD_WriteString_8x16(Label[id].Left, Label[id].Top, Label[id].Caption, FONTCOLOR, BACKCOLOR);}
  else
  {LCD_WriteString_5x7(Label[id].Left, Label[id].Top, Label[id].Caption, FONTCOLOR, BACKCOLOR, Label[id].Rot, Label[id].Zoom);}
    }
 else {//���������
   if (Label[id].FontType) { 
       LCD_WriteString_8x16(Label[id].Left, Label[id].Top, Label[id].Caption, DEFCOLOR, DEFCOLOR);}
  else
  {LCD_WriteString_5x7(Label[id].Left, Label[id].Top, Label[id].Caption, DEFCOLOR, DEFCOLOR, Label[id].Rot, Label[id].Zoom);}
 }
 }
#endif

//-----------------------------------------------------------------------------
//                    ���������  CheckBox - ������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ � ������ ����������� - 16 �����
// check - �������� ���������� - ����������, ������� �� ������, 0 - �� �������, 1 - �������
//-----------------------------------------------------------------------------

#if CHBOX  // ���� ��������� �������� - �� ����� ����

extern ChBox CheckBox[CHBOX];//������� ����������
// �������� ����������
void CheckBoxCreate(u8 id, u16 x, u16 y, u8 check) {
  CheckBox[id].ID = id;
  CheckBox[id].Left = x;
  CheckBox[id].Top = y;
  CheckBox[id].Checked = check;
  CheckBoxShow(id);
  
}


void CheckBoxShow(u8 id) {
 if (CheckBox[id].Checked) 
              {
              LCD_Draw_Rectangle(CheckBox[id].Left, CheckBox[id].Top, CheckBox[id].Left+15, CheckBox[id].Top+15, BORDKCOLOR, 0);
              LCD_Draw_Rectangle(CheckBox[id].Left+1, CheckBox[id].Top+1, CheckBox[id].Left+14, CheckBox[id].Top+14, BORDKCOLOR, 0); 
              LCD_Draw_Line(CheckBox[id].Left+3, CheckBox[id].Top+11, CheckBox[id].Left+7, CheckBox[id].Top+4, BORDKCOLOR); 
              LCD_Draw_Line(CheckBox[id].Left+7, CheckBox[id].Top+4, CheckBox[id].Left+11, CheckBox[id].Top+9, BORDKCOLOR);
              LCD_Draw_Line(CheckBox[id].Left+4, CheckBox[id].Top+11, CheckBox[id].Left+7, CheckBox[id].Top+5, BORDKCOLOR); 
              LCD_Draw_Line(CheckBox[id].Left+7, CheckBox[id].Top+5, CheckBox[id].Left+11, CheckBox[id].Top+8, BORDKCOLOR);}
      
 else    {
              LCD_Draw_Line(CheckBox[id].Left+3, CheckBox[id].Top+11, CheckBox[id].Left+7, CheckBox[id].Top+4, DEFCOLOR); 
              LCD_Draw_Line(CheckBox[id].Left+7, CheckBox[id].Top+4, CheckBox[id].Left+11, CheckBox[id].Top+9, DEFCOLOR);
              LCD_Draw_Line(CheckBox[id].Left+4, CheckBox[id].Top+11, CheckBox[id].Left+7, CheckBox[id].Top+5, DEFCOLOR); 
              LCD_Draw_Line(CheckBox[id].Left+7, CheckBox[id].Top+5, CheckBox[id].Left+11, CheckBox[id].Top+8, DEFCOLOR);
              LCD_Draw_Line(CheckBox[id].Left+4, CheckBox[id].Top+11, CheckBox[id].Left+7, CheckBox[id].Top+5, DEFCOLOR); 
              LCD_Draw_Line(CheckBox[id].Left+7, CheckBox[id].Top+5, CheckBox[id].Left+11, CheckBox[id].Top+8, DEFCOLOR);}
}

// �������. ���� ������ - ��������, � ��������
void CheckBoxClick(u8 id) {
  if (CheckBox[id].Checked) {CheckBox[id].Checked = FALSE;} 
  else {CheckBox[id].Checked = TRUE;}
  CheckBoxShow(id);
}

#endif


//-----------------------------------------------------------------------------
//                    ���������  RadioButton - �������������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ ��� ������ ����������� - 16 �����
// Itm - ���������� ������
// ItInd - ����� ������� ������
// clmn - ������������,  0 - �������������, 1 - �����������
//-----------------------------------------------------------------------------


#if RDBUTT  // ���� ��������� �������� - �� ����� ����
extern RdButt RadioButton[RDBUTT]; //������� ����������
// �������� ����������
void RadioButtonCreate(u8 id, u16 x, u16 y, u8 Itm, u8 ItInd, u8 clmn) {
  u8 i;
  RadioButton[id].ID = id;
  RadioButton[id].Left = x;
  RadioButton[id].Top = y;
  RadioButton[id].Items = Itm;
  RadioButton[id].ItemIndex = ItInd;
  RadioButton[id].Columns = clmn;
    
  if (!clmn) {
  for (i=0;i<Itm;i++) {
  LCD_Draw_Rectangle(RadioButton[id].Left+i*17, RadioButton[id].Top, RadioButton[id].Left+16+i*17, RadioButton[id].Top+16, BORDKCOLOR, 0);
  LCD_Draw_Rectangle(RadioButton[id].Left+1+i*17, RadioButton[id].Top+1, RadioButton[id].Left+15+i*17, RadioButton[id].Top+15, BORDKCOLOR, 0); 
  }
  }
  else {
  for (i=0;i<Itm;i++) {
  LCD_Draw_Rectangle(RadioButton[id].Left, RadioButton[id].Top+i*17, RadioButton[id].Left+16, RadioButton[id].Top+16+i*17, BORDKCOLOR, 0);
  LCD_Draw_Rectangle(RadioButton[id].Left+1, RadioButton[id].Top+1+i*17, RadioButton[id].Left+15, RadioButton[id].Top+15+i*17, BORDKCOLOR, 0); 
  }
  }
  RadioButtonShow(id);  
  
}

// �������. ���� ������ �������� ��������� ������
void RadioButtonClick(u8 id) {
  ++RadioButton[id].ItemIndex; 
  if (RadioButton[id].ItemIndex==RadioButton[id].Items) RadioButton[id].ItemIndex = 0;
  RadioButtonShow(id);
}

void RadioButtonShow(u8 id) {
  u8 i;
  if (!RadioButton[id].Columns) //�������������
  {
  for(i=0;i<RadioButton[id].Items;i++) {
  if (i==RadioButton[id].ItemIndex) {Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 3, BORDKCOLOR);
                                    Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 2, BORDKCOLOR);
                                    Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 1, BORDKCOLOR);}
      else {Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 3, DEFCOLOR);
            Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 2, DEFCOLOR);
            Lcd_Circle(RadioButton[id].Left+8+i*17, RadioButton[id].Top+8, 1, DEFCOLOR);}
          }
  }
  else //�����������
  {
  for(i=0;i<RadioButton[id].Items;i++) {
  if (i==RadioButton[id].ItemIndex) {Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 3, BORDKCOLOR);
                                  Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 2, BORDKCOLOR);
                                  Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 1, BORDKCOLOR);
            }
      else {Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 3, DEFCOLOR);
                  Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 2, DEFCOLOR);
                  Lcd_Circle(RadioButton[id].Left+8, RadioButton[id].Top+8+i*17, 1, DEFCOLOR);
           }
  } 
  } 
}
#endif

//-----------------------------------------------------------------------------
//                    ���������  Button - ������� ������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ ��� ������  - 22 � 60 �����
// text - �����
//-----------------------------------------------------------------------------


#if BUTT
extern Butt Button[BUTT];// ���� ��������� �������� - �� ����� ����
// �������� ����������
void ButtonCreate(u8 id, u16 x, u16 y, char *text) {
  u8 st;
  st = strlen(text);
  Button[id].ID = id;
  Button[id].Left = x;
  Button[id].Top = y;
  Button[id].Caption = text;
  LCD_Draw_Line(Button[id].Left+1, Button[id].Top, Button[id].Left+58, Button[id].Top, BORDKCOLOR);
  LCD_Draw_Line(Button[id].Left+1, Button[id].Top+21, Button[id].Left+58, Button[id].Top+21, BORDKCOLOR);
  LCD_Draw_Line(Button[id].Left, Button[id].Top+1, Button[id].Left, Button[id].Top+20, BORDKCOLOR);
  LCD_Draw_Line(Button[id].Left+59, Button[id].Top+1, Button[id].Left+59, Button[id].Top+20, BORDKCOLOR);
  LCD_Draw_Rectangle(Button[id].Left+1, Button[id].Top+1, Button[id].Left+58, Button[id].Top+20, BORDKCOLOR, 0);
  LCD_WriteString_5x7(Button[id].Left+28-(st/2)*6, Button[id].Top+6, Button[id].Caption, FONTCOLOR, BACKCOLOR, 0, 1);
  
}
// �������. ������� ������.

void ButtonClick(u8 id) {
  u8 st;
  st = strlen(Button[id].Caption);
  LCD_WriteString_5x7(Button[id].Left+28-(st/2)*6, Button[id].Top+6, Button[id].Caption, FONTCOLORCL, BACKCOLOR, 0, 1);
  Delay_ms(500);
  LCD_WriteString_5x7(Button[id].Left+28-(st/2)*6, Button[id].Top+6, Button[id].Caption, FONTCOLOR, BACKCOLOR, 0, 1);
  
}

void ButtonOnClick(u8 id) {
  // ���-�� ����� �������
}
#endif


//-----------------------------------------------------------------------------
//                    ���������  ProgressBar - ���������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ ��� ������ ����������� - 16 �����
// wdt - ������ ����������, ������ 16
// pos - �������
// vis - ���������. 1 - �������, 0 - �� �������
//-----------------------------------------------------------------------------


#if PRBAR
extern PrBar ProgressBar[PRBAR];// ���� ��������� �������� - �� ����� ����
// �������� ����������
void ProgressBarCreate(u8 id, u16 x, u16 y,u16 wdt, u16 pos, u8 vis) {
     ProgressBar[id].ID = id;
    ProgressBar[id].Left = x;
    ProgressBar[id].Top = y;
    ProgressBar[id].Width = wdt;
    ProgressBar[id].Position = pos;
    ProgressBar[id].Visible = vis; 
    ProgressBarShow(id, vis);
    
}
// �������� ��������� ��� ������
void ProgressBarShow(u8 id, u8 vis) {
   u16 ps, i;
    ps = ((ProgressBar[id].Width-2)*ProgressBar[id].Position/100); 
   if (vis) {
    LCD_Draw_Rectangle(ProgressBar[id].Left, ProgressBar[id].Top, ProgressBar[id].Left+ProgressBar[id].Width, ProgressBar[id].Top+16,BORDKCOLOR,0);
    LCD_Draw_Rectangle(ProgressBar[id].Left+1, ProgressBar[id].Top+1, ProgressBar[id].Left+ProgressBar[id].Width-1, ProgressBar[id].Top+15,BORDKCOLOR,0);
    
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, POSCOLOR);}
    if (ps<ProgressBar[id].Width-2) {
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ProgressBar[id].Width-2, ProgressBar[id].Top+2+i, NOPOSCOLOR); }
    }
    } else
    {
     LCD_Draw_Rectangle(ProgressBar[id].Left, ProgressBar[id].Top, ProgressBar[id].Left+ProgressBar[id].Width, ProgressBar[id].Top+16,DEFCOLOR,0);
    LCD_Draw_Rectangle(ProgressBar[id].Left+1, ProgressBar[id].Top+1, ProgressBar[id].Left+ProgressBar[id].Width-1, ProgressBar[id].Top+15,DEFCOLOR,0);
    
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, DEFCOLOR);}
    if (ps<ProgressBar[id].Width-2) {
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ProgressBar[id].Width-2, ProgressBar[id].Top+2+i, DEFCOLOR); }
    } 
    }  
}
// �������� ��������� �������
void ProgressBarOnEnter(u8 id, u16 pos) {
    u16 ps, i;
     if (ProgressBar[id].Visible) {
    ProgressBar[id].Position = pos;
    ps = ((ProgressBar[id].Width-2)*ProgressBar[id].Position/100);
    LCD_Draw_Rectangle(ProgressBar[id].Left, ProgressBar[id].Top, ProgressBar[id].Left+ProgressBar[id].Width, ProgressBar[id].Top+16,BORDKCOLOR,0);
    LCD_Draw_Rectangle(ProgressBar[id].Left+1, ProgressBar[id].Top+1, ProgressBar[id].Left+ProgressBar[id].Width-1, ProgressBar[id].Top+15,BORDKCOLOR,0);
    
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, POSCOLOR);}
    if (ps<ProgressBar[id].Width-2) {
    for(i=0;i<13;i++) {LCD_Draw_Line(ProgressBar[id].Left+ps+1, ProgressBar[id].Top+2+i, ProgressBar[id].Left+ProgressBar[id].Width-2, ProgressBar[id].Top+2+i, NOPOSCOLOR); }
    }}
}

#endif


//-----------------------------------------------------------------------------
//   ���������  SpinButton - ������ ��� ���������� ��� ���������� ��������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ ��� ������ ����������� - 16 �����
// strup - ������� UP - �����
// strdown - ������� DOWN - �����
//-----------------------------------------------------------------------------


#if SPBUTT
extern SpButt SpinButton[SPBUTT];  // ���� ��������� �������� - �� ����� ����
// �������� ����������
void SpinButtonCreate(u8 id, u16 x, u16 y, const unsigned int *strup, const unsigned int *strdown){
    SpinButton[id].ID = id;
    SpinButton[id].Left = x;
    SpinButton[id].Top = y;
    SpinButton[id].UpBmp = strup;
    SpinButton[id].DownBmp = strdown;
    SpinButtonShow(id);
}
// �������� ���������
void SpinButtonShow(u8 id){
  
      LCD_Draw_Rectangle(SpinButton[id].Left, SpinButton[id].Top, SpinButton[id].Left+16, SpinButton[id].Top+16, BORDKCOLOR, 0);
      LCD_Draw_Rectangle(SpinButton[id].Left+1, SpinButton[id].Top+1, SpinButton[id].Left+15, SpinButton[id].Top+15, BORDKCOLOR, 0);
      LCD_Draw_BMP(SpinButton[id].Left+5, SpinButton[id].Top+4, SpinButton[id].DownBmp, 0);  
      
      LCD_Draw_Rectangle(SpinButton[id].Left, SpinButton[id].Top+17, SpinButton[id].Left+16, SpinButton[id].Top+33, BORDKCOLOR, 0);
      LCD_Draw_Rectangle(SpinButton[id].Left+1, SpinButton[id].Top+18, SpinButton[id].Left+15, SpinButton[id].Top+32, BORDKCOLOR, 0);
      LCD_Draw_BMP(SpinButton[id].Left+5, SpinButton[id].Top+18+4, SpinButton[id].UpBmp ,0);
}

// �������. ���� ������ - ���-�� �������
void SpinButtonClick(u8 id, u8 click){
  
  if (click==1) {
      LCD_Draw_BMP(SpinButton[id].Left+5, SpinButton[id].Top+4, SpinButton[id].DownBmp,1);  
    }
  if (click==2) {
      LCD_Draw_BMP(SpinButton[id].Left+5, SpinButton[id].Top+18+4, SpinButton[id].UpBmp,1);
    }
  Delay_ms(500);
  SpinButtonShow(id);
   
}
              
void SpinButtonOnClick(u8 id, u8 click){
 // ���-�� ����� �������               
}

#endif


//-----------------------------------------------------------------------------
//                    ���������  BitBtn - ������ �  �������
// id - ����� ����������
// x,y - ��������� ���������� ������� ����������. ������ ��� ������  - 22 � 60 �����
// icon - ������ 9 � 9
//-----------------------------------------------------------------------------


#if BTBTN
extern BtBtn BitBtn[BTBTN];// ���� ��������� �������� - �� ����� ����
// �������� ����������
void BitBtnCreate(u8 id, u16 x, u16 y, const unsigned int *icon) {
  
  BitBtn[id].ID = id;
  BitBtn[id].Left = x;
  BitBtn[id].Top = y;
  BitBtn[id].Icon = icon;
  LCD_Draw_Line(BitBtn[id].Left+1, BitBtn[id].Top, BitBtn[id].Left+58, BitBtn[id].Top, BORDKCOLOR);
  LCD_Draw_Line(BitBtn[id].Left+1, BitBtn[id].Top+21, BitBtn[id].Left+58, BitBtn[id].Top+21, BORDKCOLOR);
  LCD_Draw_Line(BitBtn[id].Left, BitBtn[id].Top+1, BitBtn[id].Left, BitBtn[id].Top+20, BORDKCOLOR);
  LCD_Draw_Line(BitBtn[id].Left+59, BitBtn[id].Top+1, BitBtn[id].Left+59, BitBtn[id].Top+20, BORDKCOLOR);
  LCD_Draw_Rectangle(BitBtn[id].Left+1, BitBtn[id].Top+1, BitBtn[id].Left+58, BitBtn[id].Top+20, BORDKCOLOR, 0);
  LCD_Draw_BMP(BitBtn[id].Left+24, BitBtn[id].Top+6, BitBtn[id].Icon, 0); 
  
}
// �������. ���� ������ - ���-�� �������
void BitBtnClick(u8 id){
  
  LCD_Draw_BMP(BitBtn[id].Left+24, BitBtn[id].Top+6, BitBtn[id].Icon,1);  
  Delay_ms(500);
  LCD_Draw_BMP(BitBtn[id].Left+24, BitBtn[id].Top+6, BitBtn[id].Icon,0); 
   
}

void BitBtnOnClick(u8 id) {
  // ���-�� ����� �������
}
#endif