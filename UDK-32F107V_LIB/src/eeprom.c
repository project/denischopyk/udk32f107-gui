//-------------------------------------------------------------------------------------
//          ������� I2C EEPROM
//-------------------------------------------------------------------------------------
#include "eeprom.h"
//-------------------------------------------------------------------------------------
// ������������� I2C EEPROM
//-------------------------------------------------------------------------------------
void I2C_EEPROM_Init(void)
{
           I2C_InitTypeDef  I2C_InitStructure;
           GPIO_InitTypeDef  GPIO_InitStructure;

           RCC_APB1PeriphClockCmd(EEPROM_I2C_CLK,ENABLE);

           RCC_APB2PeriphClockCmd(EEPROM_I2C_GPIO_CLK| RCC_APB2Periph_AFIO , ENABLE);//

           /* ��������� ������� SCL � SDA */
           GPIO_InitStructure.GPIO_Pin =  EEPROM_I2C_SCL_PIN | EEPROM_I2C_SDA_PIN;  // ������ SCL � SDA -
           GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;                        // alternative function
           GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;                          // open drain
           GPIO_Init(EEPROM_I2C_GPIO_PORT, &GPIO_InitStructure);                    // ������������� ������� ������
           GPIO_PinRemapConfig(EEPROM_I2C_Remap, EEPROM_I2C_PINS_Remap);            // Remap I2C1

           /* ��������� ������ I2C */
           I2C_DeInit(EEPROM_I2C);                                                   // ��������� I2C
           I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;                                // ����� I2C
           I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_16_9;                     // Duty cycle 16:9
           I2C_InitStructure.I2C_OwnAddress1 = 1;                                    // ����� ������� 1
           I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;                               // ��������� �������� ACK
           I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // ����� ACK ����� 7-���������� ������
           I2C_InitStructure.I2C_ClockSpeed = I2C_Speed;                             // ��������� �������� ������ �� I2C
           I2C_Cmd(EEPROM_I2C, ENABLE);                                              // ��������� ������ I2C      
           I2C_Init(EEPROM_I2C, &I2C_InitStructure);                                 // ������������� ������ I2C
           I2C_AcknowledgeConfig(EEPROM_I2C, ENABLE);                                // ��������� �������� ACK
}
//-------------------------------------------------------------------------------------
// ��������������� I2C EEPROM
//-------------------------------------------------------------------------------------
void I2C_EEPROM_DeInit(void)
{
           GPIO_InitTypeDef  GPIO_InitStructure;

           RCC_APB1PeriphClockCmd(EEPROM_I2C_CLK,DISABLE);

           RCC_APB2PeriphClockCmd(EEPROM_I2C_GPIO_CLK| RCC_APB2Periph_AFIO , DISABLE); //

           /* ��������� ������� SCL � SDA */
           GPIO_InitStructure.GPIO_Pin =  EEPROM_I2C_SCL_PIN | EEPROM_I2C_SDA_PIN;     // ������ SCL � SDA -
           GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;                       // input floating
           GPIO_Init(EEPROM_I2C_GPIO_PORT, &GPIO_InitStructure);                       // �������������� ������� ������
           GPIO_PinRemapConfig(EEPROM_I2C_Remap, DISABLE);                             // Remap I2C1 off

           /* ��������� ������ I2C */
           I2C_DeInit(EEPROM_I2C);                                                     // ��������� I2C
}
//-------------------------------------------------------------------------------------
// ������ ����� �� I2C EEPROM
//-------------------------------------------------------------------------------------
uint8_t I2C_EEPROM_Byte_Read(uint16_t ReadAddr)
{
           uint8_t tmp;

           while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));                            // �������� ������������ ���� I2C
           I2C_GenerateSTART(I2C1, ENABLE);                                          // �������� ������� START
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));               // ������� ��������� ������ Master (EV5) � ���������� ����   
           I2C_Send7bitAddress(I2C1, EEPROM_I2C_ADDR, I2C_Direction_Transmitter);    // �������� ���.������ EEPROM 
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)); // ������� ��������� ������ Master transmitter (EV6) � ���������� ���� 
           I2C_SendData(I2C1, (uint8_t)((ReadAddr & 0xFF00) >> 8));                  // �������� �������� ����� ������ ������
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));          // ������� ��������� �������� MSB (EV8_2) � ���������� ���� 
           I2C_SendData(I2C1, (uint8_t)(ReadAddr & 0x00FF));                         // �������� �������� ����� ������ ������
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));          // ������� ��������� �������� LSB (EV8_2) � ���������� ���� 
           I2C_GenerateSTART(I2C1, ENABLE);                                          // �������� ���������� ������� START
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));               // ������� ��������� ������ Master (EV5) � ���������� ����   
           I2C_Send7bitAddress(I2C1, EEPROM_I2C_ADDR, I2C_Direction_Receiver);       // �������� ���.������ EEPROM 
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));    // ������� ��������� ������ Master receiver (EV6) � ���������� ���� 
           I2C_AcknowledgeConfig(I2C1, DISABLE);                                     // NACK ����� ��������� ����� ������   
           tmp=I2C_ReceiveData(I2C1);                                                // ��������� ������ ����� ������ �� EEPROM
           I2C_GenerateSTOP(I2C1, ENABLE);                                           // �������� ������� STOP   
           return tmp;                                                               // ���������� ���������� ���� ������
}
//-------------------------------------------------------------------------------------
// ������ ����� � I2C EEPROM
//-------------------------------------------------------------------------------------
void I2C_EEPROM_Byte_Write(uint8_t val, uint16_t WriteAddr)
{
           I2C_GenerateSTART(I2C1, ENABLE);                                          // �������� ������� START
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));               // ������� ��������� ������ Master (EV5) � ���������� ����  
           I2C_Send7bitAddress(I2C1, EEPROM_I2C_ADDR, I2C_Direction_Transmitter);    // �������� ���.������ EEPROM 
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)); // ������� ��������� ������ Master transmitter (EV6) � ���������� ���� 
           I2C_SendData(I2C1, (uint8_t)((WriteAddr & 0xFF00) >> 8));                 // �������� �������� ����� ������ ������
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));          // ������� ��������� �������� MSB (EV8_2) � ���������� ���� 
           I2C_SendData(I2C1, (uint8_t)(WriteAddr & 0x00FF));                        // �������� �������� ����� ������ ������
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));          // ������� ��������� �������� LSB (EV8_2) � ���������� ���� 
           I2C_SendData(I2C1, val);                                                  // �������� ����� ������
           while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));          // ������� ��������� �������� ����� ������ (EV8_2) � ���������� ���� 
           I2C_GenerateSTOP(I2C1, ENABLE);                                           // �������� ������� STOP 
}
//-------------------------------------------------------------------------------------
