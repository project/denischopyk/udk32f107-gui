//-----------------------------------------------------------------------------
//    ������������ ���� �������� ��� 320�240 �� ������ ����������� SSD1289
//-----------------------------------------------------------------------------
#ifdef __cplusplus
 extern "C" {
#endif
//-----------------------------------------------------------------------------
#ifndef _SSD1289
#define _SSD1289
//-----------------------------------------------------------------------------
//#include "stm32f10x_gpio.h"
//#include "stm32f10x_rcc.h"
#include "stm32f10x.h"
   
   
 typedef enum
{ Bit_RESET = 0,
  Bit_SET
}BitAction;  
   

   
//-----------------------------------------------------------------------------
// �� ������������
#define BACKLIGHT_PORT           PORTD 
#define BACKLIGHT_PIN            14 
#define CS_PORT                  PORTC                // PC.9 
#define CS_PIN                   9                // PC.9  
#define WR_PORT                  PORTC                 // PC.6
#define WR_PIN                   6                // PC.6
#define RS_PORT                  PORTC               // PC.8 
#define RS_PIN                   8                // PC.8   
#define RD_PORT                  PORTD               // PD.15 
#define RD_PIN                   15               // PD.15    
#define DATA_PORT                PORTE              // PE 

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                       ���������� �������
//-----------------------------------------------------------------------------
void LCD_Init();                                                                                  // ������������� ���
void LCD_Write_REG(u16 Adr, u16 Data);                                                            // �������� ������ � ������� ���
void LCD_Write_Command(u8 Comm);                                                                  // �������� �������� ���
u16  LCD_Read_REG(u16 Adr);                                                                       // ������ ������ �� �������� ���
void LCD_SetCursor(u16 x,u16 y);                                                                  // ��������� ������� ���
void LCD_FillScreen(u16 Color);                                                                   // ������� ������ ����� ������
void LCD_SetPoint(u16 x,u16 y,u16 Color);                                                         // ������ ����� �� ���
void LCD_WriteString_8x16(u16 x, u16 y, char *text, u16 charColor, u16 bkColor);                  // ����� �� ��� ������ ������� 8�16 ��������
void LCD_WriteChar_8x16(u16 x, u16 y, char c, u16 t_color, u16 b_color);                          // ����� �� ��� ������� ������� 8�16 ��������
void LCD_SetArea(u16 x1, u16 y1, u16 x2, u16 y2);                                                 // ����� ���� ��� ���������
void LCD_WriteChar_5x7(u16 x, u16 y, char c, u16 t_color, u16 b_color, u8 rot, u8 zoom );         // ����� �� ��� ������� ������� 5�7 ��������
void LCD_WriteString_5x7(u16 x, u16 y, char *text, u16 charColor, u16 b_color, u8 rot, u8 zoom ); // ����� �� ��� ������ ������� 5�7 ��������
void LCD_Draw_Line(u16 x1, u16 y1, u16 x2, u16 y2,u16 color);                                     // ����� �� ��� �����
void LCD_Draw_Circle(u16 cx,u16 cy,u16 r,u16 color,u8 fill);   
void Lcd_Circle(u16  Xc, u16 Yc, u16 r, u16 color);
void LCD_Draw_Rectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color,u8 fill);                        // ����� �� ��� ��������������
void LCD_Draw_Picture(u16 x0, u16 y0, const unsigned char *str);     
void LCD_Draw_BMP(u16 x0, u16 y0, const unsigned int *str, u8 clck);

//-----------------------------------------------------------------------------
//                       ���� ������
//-----------------------------------------------------------------------------
#define BLACK                0x0000 
#define WHITE                0xFFFF 
#define GRAY                 0xE79C
#define GREEN                0x07E0 
#define BLUE                 0x001F 
#define RED                  0xF800 
#define SKY                  0x5D1C 
#define YELLOW               0xFFE0 
#define MAGENTA              0xF81F
#define CYAN                 0x07FF
#define ORANGE               0xFCA0 
#define PINK                 0xF97F
#define BROWN                0x8200
#define VIOLET               0x9199
#define SILVER               0xA510
#define GOLD                 0xA508
#define BEGH                 0xF77B
#define NAVY		     0x000F      
#define DARK_GREEN	     0x03E0      
#define DARK_CYAN	     0x03EF      
#define MAROON		     0x7800      
#define PURPLE		     0x780F      
#define OLIVE		     0x7BE0      
#define LIGHT_GREY	     0xC618      
#define DARK_GREY	     0x7BEF
//-----------------------------------------------------------------------------
#endif /* _SSD1289 */
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif /* _cplusplus*/
//-----------------------------------------------------------------------------