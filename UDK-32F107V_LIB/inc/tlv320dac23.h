//-------------------------------------------------------------------------------------
//          ������������ ���� �������� �����-��� TLV320DAC23
//-------------------------------------------------------------------------------------


#define TLV320_CS_LOW                                     // ����� CS ���������� � "�����"
#define TLV320_Default_Volume         0x60                // ��������� �� ���������
//-------------------------------------------------------------------------------------
/* ��������� ���� I2C */
//-------------------------------------------------------------------------------------
#ifdef  TLV320_CS_LOW
#define TLV320_I2C_ADDR               0x1A                // ����� ������, ���� ����� CS ���������� � "�����"
#else
#define TLV320_I2C_ADDR               0x1B                // ����� ������, ���� ����� CS ���������� � +U���
#endif

#define TLV320_I2C_Speed              400000              // �������� 400��� 
#define TLV320_I2C                    I2C1                // ���������� I2C1
#define TLV320_I2C_CLK                RCC_APB1Periph_I2C1 // ������������ I2C

#define TLV320_I2C_Remap              GPIO_Remap_I2C1             // Remap I2C
#define TLV320_I2C_PINS_Remap         ENABLE                      // Remap �������

#define TLV320_I2C_SCL_PIN            GPIO_Pin_8                  // PB.8 
#define TLV320_I2C_SDA_PIN            GPIO_Pin_9                  // PB.9 
#define TLV320_I2C_GPIO_PORT          GPIOB                       // GPIOB 
#define TLV320_I2C_GPIO_CLK           RCC_APB2Periph_GPIOB
//-------------------------------------------------------------------------------------
/* ��������� ���� I2S */
//-------------------------------------------------------------------------------------
#define TLV320_SPI                    SPI3                        // ���������� ������ SPI1
#define TLV320_SPI_CLK                RCC_APB1Periph_SPI3         // ������������ SPI

#define TLV320_SPI_Remap              GPIO_Remap_SPI3             // Remap SPI
#define TLV320_SPI_PINS_Remap         ENABLE                      // Remap �������

#define TLV320_I2S_CK_PIN             GPIO_Pin_10                 // PC.10 
#define TLV320_I2S_CK_GPIO_PORT       GPIOC                       // GPIOC 
#define TLV320_I2S_CK_GPIO_CLK        RCC_APB2Periph_GPIOC        

#define TLV320_I2S_SD_PIN             GPIO_Pin_12                 // PC.12 
#define TLV320_I2S_SD_GPIO_PORT       GPIOC                       // GPIOC 
#define TLV320_I2S_SD_GPIO_CLK        RCC_APB2Periph_GPIOC

#define TLV320_I2S_MCK_PIN            GPIO_Pin_7                  // PC.7 
#define TLV320_I2S_MCK_GPIO_PORT      GPIOC                       // GPIOC 
#define TLV320_I2S_MCK_GPIO_CLK       RCC_APB2Periph_GPIOC

#define TLV320_I2S_WS_PIN             GPIO_Pin_4                  // PA.4 
#define TLV320_I2S_WS_GPIO_PORT       GPIOA                       // GPIOA 
#define TLV320_I2S_WS_GPIO_CLK        RCC_APB2Periph_GPIOA

/* ��� ��������� */
#define TLV320_I2S_MISO_PIN           GPIO_Pin_11                 // PC.11 
#define TLV320_I2S_MISO_GPIO_PORT     GPIOC                       // GPIOC 
#define TLV320_I2S_MISO_GPIO_CLK      RCC_APB2Periph_GPIOC
//-------------------------------------------------------------------------------------
/* �������� �������� ������ � �� ���� */
//-------------------------------------------------------------------------------------
#define TLV320_REG_LINVOL		0x00    // Left line input channel volume control:

#define LINVOL_LRS                     (1 << 8) // Left/right line simultaneous volume/mute update: 0 = Disabled, 1 = Enabled 
#define LINVOL_LIM                     (1 << 7) // Left line input mute : 0 = Normal 1 = Muted
#define LINVOL_LIV4                    (1 << 4) // Left line input volume control :
#define LINVOL_LIV3                    (1 << 3) // (10111 = 0 dB default),
#define LINVOL_LIV2                    (1 << 2) // 11111 = +12 dB down 
#define LINVOL_LIV1                    (1 << 1) // to 00000 = �34.5 dB 
#define LINVOL_LIV0                    (1 << 0) // in 1.5-dB steps
//-------------------------------------------------------------------------------------
#define TLV320_REG_RINVOL		0x01    // Right line input channel volume control:

#define RINVOL_RLS                     (1 << 8) // Right/left line simultaneous volume/mute update: 0 = Disabled, 1 = Enabled  
#define RINVOL_RIM                     (1 << 7) // Right line input mute : 0 = Normal, 1 = Muted
#define RINVOL_RIV4                    (1 << 4) // Right line input volume control :
#define RINVOL_RIV3                    (1 << 3) // (10111 = 0 dB default),
#define RINVOL_RIV2                    (1 << 2) // 11111 = +12 dB down 
#define RINVOL_RIV1                    (1 << 1) // to 00000 = �34.5 dB 
#define RINVOL_RIV0                    (1 << 0) // in 1.5-dB steps
//-------------------------------------------------------------------------------------
#define TLV320_REG_LOUTVOL		0x02    // Left channel headphone volume control:

#define LOUTVOL_LRS                    (1 << 8) // Left/right headphone channel simultaneous volume/mute update: 0 = Disabled, 1 = Enabled
#define LOUTVOL_LZC                    (1 << 7) // Left-channel zero-cross detect: 0 = Off, 1 = On
#define LOUTVOL_LHV6                   (1 << 6) // Left Headphone volume control (1111001 = 0 dB default),
#define LOUTVOL_LHV5                   (1 << 5) // 1111111 = +6 dB,  
#define LOUTVOL_LHV4                   (1 << 4) // 79 steps between +6 dB 
#define LOUTVOL_LHV3                   (1 << 3) // and �73 dB (mute),
#define LOUTVOL_LHV2                   (1 << 2) // 0110000 = �73 dB (mute), 
#define LOUTVOL_LHV1                   (1 << 1) // anything below 0110000 does nothing �
#define LOUTVOL_LHV0                   (1 << 0) // you are still muted
//-------------------------------------------------------------------------------------
#define TLV320_REG_ROUTVOL		0x03    // Right channel headphone volume control:

#define ROUTVOL_RLS                    (1 << 8) // Right/left headphone channel simultaneous volume/mute update: 0 = Disabled, 1 = Enabled
#define ROUTVOL_RZC                    (1 << 7) // Right-channel zero-cross detect: 0 = Off, 1 = On
#define ROUTVOL_RHV6                   (1 << 6) // Right Headphone volume control (1111001 = 0 dB default),
#define ROUTVOL_RHV5                   (1 << 5) // 1111111 = +6 dB,  
#define ROUTVOL_RHV4                   (1 << 4) // 79 steps between +6 dB 
#define ROUTVOL_RHV3                   (1 << 3) // and �73 dB (mute),
#define ROUTVOL_RHV2                   (1 << 2) // 0110000 = �73 dB (mute), 
#define ROUTVOL_RHV1                   (1 << 1) // anything below 0110000 does nothing �
#define ROUTVOL_RHV0                   (1 << 0) // you are still muted
//-------------------------------------------------------------------------------------
#define TLV320_REG_AN_PATH		0x04    // Analog audio path control:

#define AN_PATH_TONE_0                  0x120   // Added sidetone 0dB
#define AN_PATH_TONE_6                  0x20    // Added sidetone -6dB
#define AN_PATH_TONE_9                  0x60    // Added sidetone -9dB
#define AN_PATH_TONE_12                 0xA0    // Added sidetone -12dB
#define AN_PATH_TONE_18                 0xE0    // Added sidetone -18dB
#define AN_PATH_TONE_OFF                0x1F    // Added sidetone off

#define AN_PATH_DAC                    (1 << 4) // DAC select: 0 = DAC off, 1 = DAC selected
#define AN_PATH_BYP                    (1 << 3) // Bypass: 0 = Disabled, 1 = Enabled
#define AN_PATH_INSEL                  (1 << 2) // Input select for ADC: 0 = Line, 1 = Microphone
#define AN_PATH_MICM                   (1 << 1) // Microphone mute: 0 = Normal, 1 = Muted
#define AN_PATH_MICB                   (1 << 0) // Microphone boost: 0= 0dB, 1 = 20dB
//-------------------------------------------------------------------------------------
#define TLV320_REG_DIG_PATH	        0x05    // Digital audio path control:

#define DIG_PATH_DACM                  (1 << 3) // DAC soft mute: 0 = Disabled, 1 = Enabled
#define DIG_PATH_DEEMP1                (1 << 2) // De-emphasis control: 00 = Disabled, 
#define DIG_PATH_DEEMP0                (1 << 1) // 01 = 32 kHz, 10 = 44.1 kHz, 11 = 48 kHz
#define DIG_PATH_ADCHP                 (1 << 0) // ADC high-pass filter: 0 = Disabled, 1 = Enabled
//-------------------------------------------------------------------------------------
#define TLV320_REG_POWER		0x06    // Power down control:

#define POWER_OFF                      (1 << 7) // Device power: 0 = On, 1 = Off
#define POWER_CLK                      (1 << 6) // Clock: 0 = On, 1 = Off
#define POWER_OSC                      (1 << 5) // Oscillator: 0 = On, 1 = Off
#define POWER_OUT                      (1 << 4) // Outputs: 0 = On, 1 = Off
#define POWER_DAC                      (1 << 3) // DAC: 0 = On, 1 = Off
#define POWER_ADC                      (1 << 2) // ADC: 0 = On, 1 = Off
#define POWER_MIC                      (1 << 1) // Microphone input: 0 = On, 1 = Off
#define POWER_LINE                     (1 << 0) // Line input: 0 = On, 1 = Off
//-------------------------------------------------------------------------------------
#define TLV320_REG_DIG_FORMAT	        0x07    // Digital audio interface format:

#define DIG_FORMAT_MS                  (1 << 6) // Master/slave mode: 0 = Slave, 1 = Master
#define DIG_FORMAT_LRSWAP              (1 << 5) // DAC left/right swap: 0 = Disabled, 1 = Enabled
#define DIG_FORMAT_LRP                 (1 << 4) // DAC left/right phase: 0 = Right channel on, LRCIN high; 1 = Right channel on, LRCIN low
                                                // DSP mode
                                                // 1 = MSB is available on 2nd BCLK rising edge after LRCIN rising edge
                                                // 0 = MSB is available on 1st BCLK rising edge after LRCIN rising edge
#define DIG_FORMAT_IWL1                (1 << 3) // Input bit length: 00 = 16 bit, 
#define DIG_FORMAT_IWL0                (1 << 2) // 01 = 20 bit, 10 = 24 bit, 11 = 32 bit
#define DIG_FORMAT_FOR1                (1 << 1) // Data format: 11 = DSP format, frame sync followed by two data words
#define DIG_FORMAT_FOR0                (1 << 0) // 10 = I2S format, MSB first, left � aligned
                                                // 01 = MSB first, left aligned
                                                // 00 = MSB first, right aligned
//-------------------------------------------------------------------------------------
#define TLV320_REG_SRATE		0x08    // Sample rate control:

#define SRATE_CLKOUT                   (1 << 7) // Clock output divider: 0 = MCLK, 1 = MCLK/2
#define SRATE_CLKIN                    (1 << 6) // Clock input divider: 0 = MCLK, 1 = MCLK/2
#define SRATE_SR3                      (1 << 5) // Sampling rate control 
#define SRATE_SR2                      (1 << 4) // 
#define SRATE_SR1                      (1 << 3) // 
#define SRATE_SR0                      (1 << 2) // 
#define SRATE_BOSR                     (1 << 1) // Base oversampling rate:
                                                // USB mode: 0 = 250fs, 1 = 272fs
                                                // Normal mode: 0 = 256fs, 1 = 384fs
#define SRATE_USB                      (1 << 0) // USB/Normal Clock mode select: 0 = Normal, 1 = USB
//-------------------------------------------------------------------------------------
#define TLV320_REG_DIG_ACT		0x09    // Digital interface activation:

#define DIG_ACT_ACT                    (1 << 0) // Activate interface: 0 = Inactive, 1 = Active
//-------------------------------------------------------------------------------------
#define TLV320_REG_RESET		0x0F    // Reset register: Write 0x000 to this register triggers reset
//-------------------------------------------------------------------------------------
typedef unsigned char TVolume;
typedef enum {Normal, Mute} TMute;
typedef enum {Slave, Master} TMode;
typedef enum {OFF, ON} TSwitch;
typedef enum {fs_250, fs_272, fs_256, fs_384} TBOSR;
typedef enum {SR_8000, SR_8021, SR_32000, SR_44100, SR_48000, SR_88200, SR_96000} TSampleRate;
typedef enum {Right_Hi, Right_Low} TLRCIN_Phase; 
typedef enum {deemp_Disabled, deemp_32, deemp_441, deemp_48} TDEEMP;
typedef struct
{
  TVolume Left_LineIn_Volume;  // ������� ��������� ������ ������ "Line in"
  TSwitch LR_LineIn_Simult;    // Left/right "Line in" simultaneous volume/mute update
  TMute Left_LineIn_Mute;      // ����� "Mute" ������ ������ "Line in"
  
  TVolume Right_LineIn_Volume; // ������� ��������� ������� ������ "Line in"
  TSwitch RL_LineIn_Simult;    // Right/left "Line in" simultaneous volume/mute update
  TMute Right_LineIn_Mute;     // ����� "Mute" ������� ������ "Line in"
  
  TVolume Left_Out_Volume;     // ������� ��������� ������ ������ "Out"
  TSwitch LR_Out_Simult;       // Left/right "Out" channel simultaneous volume/mute update
  TSwitch Left_Zero_Detect;    // Left-channel zero-cross detect
  
  TVolume Right_Out_Volume;    // ������� ��������� ������� ������ "Out"
  TSwitch RL_Out_Simult;       // Right/left "Out" channel simultaneous volume/mute Update
  TSwitch Right_Zero_Detect;   // Right-channel zero-cross detect
  
  TSwitch DAC_Enable;          // DAC select
  TSwitch Bypass_Enable;       // Bypass
  
  TSwitch DAC_Mute;            // DAC soft mute
  
  TDEEMP  DEEMP;               // De-emphasis control
  
  TSwitch Power;               // Power
  TSwitch Clock;               // Clock 
  TSwitch Oscillator;          // Oscillator
  TSwitch Outputs;             // Outputs
  TSwitch DAC_Power;           // DAC 
  TSwitch LineIn_Power;        // Line In
  
  TMode TLV320_Mode;           // Master/slave mode
  TSwitch DAC_LR_Swap;         // DAC left/right swap
  TLRCIN_Phase LRCIN_Phase;    // DAC left/right phase
  
  
  
  
  
  
  
  TSampleRate SampleRate;      // Bitrate
  TBOSR BOSR;                  // Base oversampling rate
                               // USB mode: 250 fs / 272 fs
                               // Normal mode: 256 fs / 384 fs
  
  TSwitch Interface;           // Digital interface
} Audio_InitTypeDef ;




//-------------------------------------------------------------------------------------
// ������� ������ � ������������ TLV3320AIC23
//-------------------------------------------------------------------------------------
void TLV320_WriteRegister (unsigned char reg, unsigned short value); // ������ ������ � ������� ������
void TLV320_Init (void);                                             // ������������� ������
int TLV320_Set_Sample_Rate(unsigned long srate);                     // ��������� ��������