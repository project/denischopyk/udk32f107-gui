//-------------------------------------------------------------------------------------
//          ������������ ���� �������� I2C EEPROM
//-------------------------------------------------------------------------------------
#ifdef __cplusplus
 extern "C" {
#endif
//-------------------------------------------------------------------------------------
#include "stm32f10x_i2c.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
//-------------------------------------------------------------------------------------
/* ��������� ���� I2C */
//-------------------------------------------------------------------------------------
#define EEPROM_I2C_ADDR               0xA0                   // ����� I2C EEPROM
#define I2C_Speed                     400000                 // �������� 400���   
   
#define EEPROM_I2C_SCL_PIN            GPIO_Pin_8                  // PB.8 
#define EEPROM_I2C_SDA_PIN            GPIO_Pin_9                  // PB.9 
#define EEPROM_I2C_GPIO_PORT          GPIOB                       // GPIOB 
#define EEPROM_I2C_GPIO_CLK           RCC_APB2Periph_GPIOB
#define EEPROM_I2C                    I2C1                // ���������� I2C1
#define EEPROM_I2C_CLK                RCC_APB1Periph_I2C1 // ������������ I2C
#define EEPROM_I2C_Remap              GPIO_Remap_I2C1             // Remap I2C
#define EEPROM_I2C_PINS_Remap         ENABLE                      // Remap �������
//-------------------------------------------------------------------------------------
// ������� ������ � I2C EEPROM
//-------------------------------------------------------------------------------------
void I2C_EEPROM_Init(void);                                  // ������������� I2C EEPROM
void I2C_EEPROM_DeInit(void);                                // ��������������� I2C EEPROM
uint8_t I2C_EEPROM_Byte_Read(uint16_t ReadAddr);             // ������ ����� �� I2C EEPROM
void I2C_EEPROM_Byte_Write(uint8_t val, uint16_t WriteAddr); // ������ ����� � I2C EEPROM
//-------------------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif
//-------------------------------------------------------------------------------------