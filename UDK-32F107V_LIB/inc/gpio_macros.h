/*******************************************************************************
    �������� ������� �������� �������� � GPIO STM32.
*******************************************************************************/
#ifndef gpio_macros_H
#define gpio_macros_H

#include "stm32F10x.h"
/*      ������� ������ GPIO     */
#define PORTA   ((u32*)(APB2PERIPH_BASE + 0x0800))
#define PORTB   ((u32*)(APB2PERIPH_BASE + 0x0C00))
#define PORTC   ((u32*)(APB2PERIPH_BASE + 0x1000))
#define PORTD   ((u32*)(APB2PERIPH_BASE + 0x1400))
#define PORTE   ((u32*)(APB2PERIPH_BASE + 0x1800))
#define PORTF   ((u32*)(APB2PERIPH_BASE + 0x1C00))
#define PORTG   ((u32*)(APB2PERIPH_BASE + 0x2000))


/*------------------------------------------------------------------------------
    ������� ��� GPIO.
    ����� �������� �� ������� ������� ��������� �:
    (port) + 0 = GPIOx_CRL, + 1 = GPIOx_CRH, + 2 = GPIOx_IDR, + 3 = GPIOx_ODR, + 4 = GPIOx_BSRR, + 5 = BRR
------------------------------------------------------------------------------*/

// Set pin - ��������� ����� ����� �� �����. ��������: spin_m (PORTC,(1<<4)|((1<<5)); // ���������� PC4, PC5.
#define spin_m(port,mask) (*(port+4)=(u32)(mask))

// Clear pin - ����� ����� ����� �� �����. ��������: �pin_m (PORT�,(1<<0)|((1<<1)); // �������� PB0, PB1.
#define cpin_m(port,mask) (*(port+4)=(u32)((mask)<<16))

// Invert pin - �������� ������ �����. ��������: ipin_m (PORTC,((1<<5)); // �������� ��������� PC5.
#define ipin_m(port,mask) (*(port+3)^=(u32)(mask)) 

// Write port - ������ ��������� �������� ����� ODR. ��������: wr_port(PORTC, 0xFFFF); // �������� 0xFFFF � GPIOC->ODR.
#define wr_port(port,word) (*(port+3)=(u32)(word))

// Read port - ������ �������� �������� ����� IDR. ��������: tmp=rd_port(PORTA); // ������� GPIOA->IDR � tmp.
#define rd_port(port) (*(port+2))

// Get pin - �������� ������ �����. ��������: if (getpin_b(PORTA, 0)) // ���� ����� PA.0 ����������...
#define getpin(port,bit) (*(port+2)&(1<<(bit)))

// Set pin - ������ �����. ��������: setpin_b(PORTA, 0) // 
#define setpin(port,bit) (*(port+4)|=(1<<(bit)))

// Reset pin - ������ �����. ��������: resetpin_b(PORTA, 0) // 
#define resetpin(port,bit) (*(port+5)|= (1<<(bit)))

// Output Push Pull - ��������� ������ b �� ����������� ����� (MODE=11, CNF=00). ��������: pinOutPP_b (PORTC,8); 
#define pinOutPP_50M(port,bit)  {*(port+((bit)/8))|=3UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Open Drain - ����� b - �� ����� c �������� ������ (MODE=11, CNF=01). ��������: pinOutOD_b (PORTA,0); 
#define pinOutOD_50M(port,bit)  {*(port+((bit)/8))|=3UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(1UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function - ������ b - ����������� ����� c �������������� �������� (MODE=11, CNF=10). ��������: pinOutAF_b (PORTB,14); 
#define pinOutAF_50M(port,bit)  {*(port+((bit)/8))|=3UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(2UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function & Open Drain - ������ b - C �������� ������ � �������������� �������� (MODE=11, CNF=11). ��������: pinOutAFOD_b (PORTB,14); 
#define pinOutAFOD_50M(port,bit)  {*(port+((bit)/8))|=3UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}


// Output Push Pull - ��������� ������ b �� ����������� ����� (MODE=01, CNF=00). ��������: pinOutPP_b (PORTC,8); 
#define pinOutPP_10M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 1UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Open Drain - ����� b - �� ����� c �������� ������ (MODE=01, CNF=01). ��������: pinOutOD_b (PORTA,0); 
#define pinOutOD_10M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 1UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(1UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function - ������ b - ����������� ����� c �������������� �������� (MODE=01, CNF=10). ��������: pinOutAF_b (PORTB,14); 
#define pinOutAF_10M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|=1UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(2UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function & Open Drain - ������ b - C �������� ������ � �������������� �������� (MODE=01, CNF=11). ��������: pinOutAFOD_b (PORTB,14); 
#define pinOutAFOD_10M(port,bit) {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|=1UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|=(3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}



// Output Push Pull - ��������� ������ b �� ����������� ����� (MODE=10, CNF=00). ��������: pinOutPP_b (PORTC,8); 
#define pinOutPP_2M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 2UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Open Drain - ����� b - �� ����� c �������� ������ (MODE=10, CNF=01). ��������: pinOutOD_b (PORTA,0); 
#define pinOutOD_2M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 2UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (1UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function - ������ b - ����������� ����� c �������������� �������� (MODE=10, CNF=10). ��������: pinOutAF_b (PORTB,14); 
#define pinOutAF_2M(port,bit)   {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 2UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (2UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Output Alternative Function & Open Drain - ������ b - C �������� ������ � �������������� �������� (MODE=10, CNF=11). ��������: pinOutAFOD_b (PORTB,14); 
#define pinOutAFOD_2M(port,bit) {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))|= 2UL<<(((bit)-(8*((bit)/8)))<<2); *(port+((bit)/8))&=~ (3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (3UL<<((((bit)-(8*((bit)/8)))<<2)+2));}

// Input analog - ����� b - ���� � ������� Z-��������� (MODE=00, CNF=00).  // ����� PA0 �� ����. 
#define pinInputAn(port,bit)  {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); }


// Input Z-state - ����� b - ���� � ������� Z-��������� (MODE=00, CNF=01). pinOutZ (PORTA,0); // ����� PA0 �� ����. 
#define pinInputZ(port,bit)  {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (1UL<<((((bit)-(8*((bit)/8)))<<2)+2)); }

// Input Pull Up - ����� b - ���� c ���������� �� ���� ������� (MODE=00, CNF=10, ODR=1). pinInputPU_b (PORTD,12); // ����� PD12 - PullUp ����. 
#define pinInputPU(port,bit)  {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (2UL<<((((bit)-(8*((bit)/8)))<<2)+2)); (*(port+3)|=(1<<(bit)));}

// Input Pull Down - ����� b - ���� c ���������� �� ����� (MODE=00, CNF=10, ODR=0). pinInputPD_b (PORTD,5); // ����� PD5 - PullDown ����. 
#define pinInputPD(port,bit)  {*(port+((bit)/8))&= ~(3UL<<(((bit)-(8*((bit)/8)))<<2)); *(port+((bit)/8))&= ~(3UL<<((((bit)-(8*((bit)/8)))<<2)+2)); *(port+((bit)/8))|= (2UL<<((((bit)-(8*((bit)/8)))<<2)+2)); (*(port+3)&= ~(1<<(bit)));}



// Input floating - port ���������� (MODE=00, CNF=00) // 
#define portInputAn(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000;}

// Input Z-state - port � ������� Z-��������� (MODE=00, CNF=01)); // 
#define portInputFl(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x44444444; *(port+1) |= 0x44444444;}

// Input  Pull Up - ���� � ��������� � ������� (MODE=00, CNF=10, ODR = 1)); // 
#define portInputPU(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x88888888; *(port+1) |= 0x88888888; *(port+3) |= 0xFFFF;}


// Input  Pull Down - ���� � ��������� � ������� (MODE=00, CNF=10, ODR = 0)); // 
#define portInputPD(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x88888888; *(port+1) |= 0x88888888; *(port+3) &= ~0xFFFF;}


// Output push-pull - 10 MHz (MODE=01, CNF=00) // 
#define portOutPP_10M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x11111111; *(port+1) |= 0x11111111;}

// Output open-drain - 10 MHz (MODE=01, CNF=01) // 
#define portOutOD_10M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x55555555; *(port+1) |= 0x55555555;}

// Output altern push-pull - 10 MHz (MODE=01, CNF=10) // 
#define portOutAPP_10M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x99999999; *(port+1) |= 0x99999999;}

// Output altern open-drain - 10 MHz (MODE=01, CNF=11) // 
#define portOutAOD_10M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0xDDDDDDDD; *(port+1) |= 0xDDDDDDDD;}


// Output push-pull - 2 MHz (MODE=01, CNF=00) // 
#define portOutPP_2M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x22222222; *(port+1) |= 0x22222222;}

// Output open-drain - 2 MHz (MODE=01, CNF=01) // 
#define portOutOD_2M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x66666666; *(port+1) |= 0x66666666; }

// Output altern push-pull - 2 MHz (MODE=01, CNF=10) // 
#define portOutAPP_2M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0xAAAAAAAA; *(port+1) |= 0xAAAAAAAA;}

// Output altern open-drain - 2 MHz (MODE=01, CNF=11) // 
#define portOutAOD_2M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0xEEEEEEEE; *(port+1) |= 0xEEEEEEEE; }

// Output push-pull - 50 MHz (MODE=01, CNF=00) // 
#define portOutPP_50M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x33333333; *(port+1) |= 0x33333333;}

// Output open-drain - 50 MHz (MODE=01, CNF=01) // 
#define portOutOD_50M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0x77777777; *(port+1) |= 0x77777777; }

// Output altern push-pull - 50 MHz (MODE=01, CNF=10) // 
#define portOutAPP_50M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0xBBBBBBBB; *(port+1) |= 0xBBBBBBBB; }

// Output altern open-drain - 50 MHz (MODE=01, CNF=11) // 
#define portOutAOD_50M(port)  {*(port) &= 0x00000000; *(port+1) &= 0x00000000; *(port) |= 0xFFFFFFFF; *(port+1) |= 0xFFFFFFFF; }


#endif

