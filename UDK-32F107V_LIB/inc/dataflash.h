//------------------------------------------------------------------------------
//          ������������ ���� �������� Dataflash
//------------------------------------------------------------------------------
#ifdef __cplusplus
 extern "C" {
#endif
//------------------------------------------------------------------------------
#ifndef _DATAFLASH
#define _DATAFLASH
//------------------------------------------------------------------------------
#define DATAFLASH_AUTODETECT // ��������������� ��� ������, ���� �� ����� ��������������� Dataflash
#define DATAFLASH_LINEAR_SPACE
//------------------------------------------------------------------------------
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"
//------------------------------------------------------------------------------
// ������� ���������� Dataflash
//------------------------------------------------------------------------------
#define Dataflash_SPI                    SPI1                        // ���������� ������ SPI1
#define Dataflash_SPI_CLK                RCC_APB2Periph_SPI1         // ������������ SPI
#define Dataflash_SPI_Remap              GPIO_Remap_SPI1             // Remap SPI
#define Dataflash_SPI_PINS_Remap         DISABLE                     // Remap ��������
#define Dataflash_SPI_SCK_PIN            GPIO_Pin_5                  // PA.5 
#define Dataflash_SPI_SCK_GPIO_PORT      GPIOA                       // GPIOA 
#define Dataflash_SPI_SCK_GPIO_CLK       RCC_APB2Periph_GPIOA
#define Dataflash_SPI_MISO_PIN           GPIO_Pin_6                  /* PA.6 */
#define Dataflash_SPI_MISO_GPIO_PORT     GPIOA                       /* GPIOA */
#define Dataflash_SPI_MISO_GPIO_CLK      RCC_APB2Periph_GPIOA
#define Dataflash_SPI_MOSI_PIN           GPIO_Pin_7                  /* PA.7 */
#define Dataflash_SPI_MOSI_GPIO_PORT     GPIOA                       /* GPIOA */
#define Dataflash_SPI_MOSI_GPIO_CLK      RCC_APB2Periph_GPIOA
#define Dataflash_CS_PIN                 GPIO_Pin_5                  /* PB.05 */
#define Dataflash_CS_GPIO_PORT           GPIOB                       /* GPIOB */
#define Dataflash_CS_GPIO_CLK            RCC_APB2Periph_GPIOB
//------------------------------------------------------------------------------
// �������� ���������� ������������� Dataflash
//------------------------------------------------------------------------------
typedef struct 
{
  u16  Pages;       // ���������� �������
  u16  Page_Size;   // ������ �������� � ������
  u8   Page_Bit;    // ����� ������ ��������
  u8   Chip_ID;     // ��� ����������
} TDataflash_Info;
//------------------------------------------------------------------------------
// ��� ������ ��� ������ �� �������
//------------------------------------------------------------------------------
typedef enum
{
  OK,
  ERR
} result;
//------------------------------------------------------------------------------
// ������� 
//------------------------------------------------------------------------------
result Dataflash_Init();                                                    // ������������� Dataflash
void   Dataflash_Deinit();                                                  // ��������������� Dataflash
u8     Dataflash_isReady();                                                 // ���������, ������ �� Dataflash
u8     Dataflash_GetByte(u8 BuferNo, u16 Addr );                            // ������ ����� �� SRAM-������ Dataflash
void   Dataflash_GetBlock(u8 BuferNo, u16 Addr, u16 Count, u8 *BuferPtr);   // ������ ����� �� SRAM-������ Dataflash
void   Dataflash_WriteByte(u8 BuferNo, u16 Addr, u8 Data);                  // ������ ����� � SRAM-����� Dataflash
void   Dataflash_WriteBlock(u8 BuferNo, u16 Addr, u16 Count, u8 *BuferPtr); // ������ ����� � SRAM-����� Dataflash
void   Dataflash_FlashRead(u16 PageAdr, u16 Addr, u16 Count, u8 *BuferPtr); // ������ ����� �� FLASH-������ Dataflash
void   Dataflash_PageFunc(u8 PageCmd, u16 PageAdr );                        // ����������� �� ���������� Dataflash
//------------------------------------------------------------------------------
// ������� ������ � �������� ����������� Dataflash
//------------------------------------------------------------------------------
void Dataflash_Linear_GoToZero();                                           // ��������� ��������� �� ������ ������
u32  Dataflash_Linear_SpaceToEnd();                                         // ���������� ���������� ���� �� ����� dataflash.
void Dataflash_Linear_Read(u8 *bufer, u16 count);                           // ������ ������
void Dataflash_Linear_UnsafeWrite(u8 *bufer, u16 count);                    // ������ ������
void Dataflash_Linear_Finalize();                                           // ��������� ����� ������ � ���������� ������
void Dataflash_Linear_SeekForward(u32 displacement);                        // ����������� ��������� ���������� ������
void Dataflash_Linear_SeekBackward(u32 displacement);                       // ����������� ��������� ���������� �����
void Dataflash_Linear_GoTo(u32 location);                                   // ����������� ��������� � �������� �����
//------------------------------------------------------------------------------
//  ���� ������� ��� ������������� � Dataflash_PageFunc
//------------------------------------------------------------------------------
#define DF_FLASH_TO_BUF1 		0x53	/* Main memory page to buffer 1 transfer  */
#define DF_FLASH_TO_BUF2 		0x55	/* Main memory page to buffer 2 transfer */
#define DF_BUF1_TO_FLASH_WITH_ERASE   	0x83	/* Buffer 1 to main memory page program with built-in erase */
#define DF_BUF2_TO_FLASH_WITH_ERASE   	0x86	/* Buffer 2 to main memory page program with built-in erase */
#define DF_BUF1_TO_FLASH_WITHOUT_ERASE  0x88	/* Buffer 1 to main memory page program without built-in erase */
#define DF_BUF2_TO_FLASH_WITHOUT_ERASE  0x89	/* Buffer 2 to main memory page program without built-in erase */
//------------------------------------------------------------------------------
#endif /* _DATAFLASH */
#ifdef __cplusplus
}
#endif /* _cplusplus*/
//------------------------------------------------------------------------------

