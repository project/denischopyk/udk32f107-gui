#include "stm32f10x.h"
#include "string.h"
#include "delay.h"


typedef enum
{ FONT5x7 = 0,
  FONT8x16 =1
}FontSise;

#define TRUE  1
#define FALSE  0

#define DEFCOLOR  WHITE // ���� ������
#define FONTCOLOR BLUE   //���� ������
#define FONTCOLORCL RED   //���� ������ ��� �������
#define BACKCOLOR WHITE //���� ����
#define BORDKCOLOR BLUE //���� ����������
#define NOPOSCOLOR   CYAN //���� ProgressBar
#define POSCOLOR   RED //���� ProgressBar


#define LABNUM  1
#define CHBOX   2
#define RDBUTT  2
#define BUTT    2
#define PRBAR   2
#define SPBUTT   2
#define BTBTN   2

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    char          *Caption;
    u8            Rot;
    u8            Zoom;
    FontSise      FontType;  
}Lab;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    u8            Checked;
}ChBox;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    u8            Items;
    u8            ItemIndex;
    u8            Columns;
}RdButt;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    char          *Caption;
}Butt;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    u16           Width;
    u16           Position;
    u8            Visible;    
}PrBar;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    const unsigned int *UpBmp;
    const unsigned int *DownBmp;
}SpButt;

typedef struct   
{ 
    u8            ID;
    u16           Left;
    u16           Top;
    const unsigned int *Icon;
    
}BtBtn;



void LabelCreate(u8 id, u16 x, u16 y, char *text,u8 rot, u8 zoom, FontSise FType);
void LabelShow(u8 id, u8 vis);

void CheckBoxCreate(u8 id, u16 x, u16 y, u8 check);
void CheckBoxShow(u8 id);
void CheckBoxClick(u8 id);



void RadioButtonCreate(u8 id, u16 x, u16 y,u8 Itm, u8 ItInd, u8 clmn);
void RadioButtonShow(u8 id);
void RadioButtonClick(u8);


void ButtonCreate(u8 id, u16 x, u16 y, char *text);
void ButtonClick(u8 id);
void ButtonOnClick(u8 id);


void ProgressBarCreate(u8 id, u16 x, u16 y, u16 wdt, u16 pos, u8 vis);
void ProgressBarShow(u8 id, u8 vis);
void ProgressBarOnEnter(u8 id, u16 pos);


void SpinButtonCreate(u8 id, u16 x, u16 y, const unsigned int *strup, const unsigned int *strdown);
void SpinButtonShow(u8 id);
void SpinButtonClick(u8 id,  u8 click);
void SpinButtonOnClick(u8 id, u8 click);

void BitBtnCreate(u8 id, u16 x, u16 y, const unsigned int *icon);
void BitBtnClick(u8 id);
void BitBtnOnClick(u8 id);

