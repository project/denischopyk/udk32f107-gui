#include "stm32f10x.h"
#include "stm32f10x_it.h"

#include "ssd1289.h"
#include "xpt2046.h"
#include "stdio.h"
#include "leds.h"
#include "delay.h"
#include "graphicTypes.h"
#include "misc.h"


#include "Logo.h"

//#define TouchScreen_IRQ_PIN_PR         EXTI_PR_PR13   // ���� ����������


#if LABNUM  
Lab Label[LABNUM];
#endif
#if CHBOX
ChBox CheckBox[CHBOX];
#endif
#if RDBUTT
RdButt RadioButton[RDBUTT];
#endif
#if BUTT
Butt Button[BUTT];
#endif
#if PRBAR
PrBar ProgressBar[PRBAR];
#endif
#if SPBUTT
SpButt SpinButton[SPBUTT];
#endif
#if BTBTN
BtBtn BitBtn[BTBTN];
#endif


volatile u8 Flag;


void SystemInit(void);
void Init_EXTI(void);


int main(void) {

 u8 i;
 
  Flag = 0;
  SystemInit ();
  LCD_Init();
  
    
  TouchScreen_Init();
  LCD_FillScreen(WHITE);
  
  TouchScreen_Calibrate();
  LCD_FillScreen(WHITE);
  
  LED_Init (BOTH_LEDS);
  LabelCreate(0, 10, 200, "label",0, 1, FONT8x16);
  LabelShow(0, 1);
  //LabelShow(0, 0);
  
  CheckBoxCreate(0, 50, 200, 1);
  CheckBoxCreate(1, 100, 200, 1);

  RadioButtonCreate(0, 50, 180, 4, 1, 0);
  RadioButtonCreate(1, 120, 180, 3, 1, 1);
    
  ButtonCreate(0, 50, 150, "�����");
  ButtonCreate(1, 120, 150, "����");
  
  BitBtnCreate(0, 50, 100, all);
  BitBtnCreate(1, 120, 100, no);

  SpinButtonCreate(0, 50, 50,  up, down);
  
  
  ProgressBarCreate(0, 120, 50, 100, 10, 1);
  Delay_ms(1000);

   
  
  Init_EXTI();
  __enable_irq ();
  
  while(1)
  {    //for(u16 j = 0; j < 500; j++ );		//200ns 
     // while (TouchScreen_IRQ() == SET);
      //if (GPIO_ReadInputDataBit(TouchScreen_IRQ_GPIO_PORT, TouchScreen_IRQ_PIN) == RESET)
      //int X=0, Y=123; 
      //TouchScreen_Read(&X, &Y);
      //LCD_SetPoint(X,Y,RED);
          //for (u8 i=0; i<25; i++) while (GPIO_ReadInputDataBit(TouchScreen_IRQ_GPIO_PORT, TouchScreen_IRQ_PIN) == RESET); 
    //Flag = TouchScreen_IRQ();
    
    // ���������� �� Touch Screen
    if(Flag) {
          NVIC_DisableIRQ (EXTI15_10_IRQn);
          int X=0, Y=123; 
          TouchScreen_Read(&X, &Y);
          //�������� CheckBox
          for (i=0; i<CHBOX;i++){ if ((CheckBox[i].Left<X)&&(X<CheckBox[i].Left+17)&&(CheckBox[i].Top<Y)&&(Y<CheckBox[i].Top+17))   
                {CheckBoxClick(i);}}
         //�������� RadioButton
          for (i=0; i<RDBUTT;i++){
            if (RadioButton[i].Columns==0) {
            if ((RadioButton[i].Left<X)&&(X<(RadioButton[i].Left+1+17*RadioButton[i].Items))&&(RadioButton[i].Top<Y)&&(Y<RadioButton[i].Top+17))   
                {RadioButtonClick(i);}  
            }
            if (RadioButton[i].Columns==1) {
            if ((RadioButton[i].Left<X)&&(X<RadioButton[i].Left+17)&&(RadioButton[i].Top<Y)&&(Y<(RadioButton[i].Top+1+17*RadioButton[i].Items)))   
                {RadioButtonClick(i);}   
            }
          }
          //�������� Button
          for (i=0; i<BUTT;i++){ if ((Button[i].Left<X)&&(X<Button[i].Left+60)&&(Button[i].Top<Y)&&(Y<Button[i].Top+22))   
                {ButtonClick(i); ButtonOnClick(i);}}
          //�������� BitBtn
          for (i=0; i<BTBTN;i++){ if ((BitBtn[i].Left<X)&&(X<BitBtn[i].Left+60)&&(BitBtn[i].Top<Y)&&(Y<BitBtn[i].Top+22))   
                {BitBtnClick(i); BitBtnOnClick(i);}}
          //�������� SpinButton
          for (i=0; i<SPBUTT;i++){ 
            if ((SpinButton[i].Left<X)&&(X<SpinButton[i].Left+16)&&(SpinButton[i].Top<Y)&&(Y<SpinButton[i].Top+16))   
                {SpinButtonClick(i, 1); SpinButtonOnClick(i, 1);}
            if ((SpinButton[i].Left<X)&&(X<SpinButton[i].Left+16)&&(SpinButton[i].Top+16<Y)&&(Y<SpinButton[i].Top+33))   
                {SpinButtonClick(i, 2); SpinButtonOnClick(i, 2);}
          }
          
          Flag = 0;
          EXTI->PR |= EXTI_PR_PR13;
          NVIC_EnableIRQ (EXTI15_10_IRQn);
           
    }
       
    LED_On(BOTH_LEDS);
   ProgressBar[0].Position += 10;
    if (ProgressBar[0].Position > 100) ProgressBar[0].Position = 0;;
   ProgressBarShow(0, 1);
    for (unsigned int i=0; i<60000; i++) {}
  
    LED_Off(BOTH_LEDS);
    for (unsigned int i=0; i<60000; i++) {}
   
   }
}



void SystemInit(void)
 {
unsigned long int TimeOut = 10000;         
//
 //��������� HSE
 RCC->CR   |=  RCC_CR_HSEON;                          //�������� ��������� HSE
 while((RCC->CR & RCC_CR_HSERDY)==0) {}               //�������� ���������� HSE
 if(TimeOut) TimeOut--;
 if(TimeOut==0) {                                     //������!!! ��������� HSE �� ����������
 //�������� ���� ������� �������� ���� HSE 
 if (RCC->CIR & RCC_CIR_CSSF) RCC->CIR |= RCC_CIR_CSSC;  
//
 //���� ���������� �����, ������ HSE �� ��������
 //���-�� ����� �����������: ������������� ���������, ���� ������ ����� � �.�.
}
 else
{
 RCC->CR   |=  RCC_CR_CSSON;                          //��������� ������ ������� ������ ���� HSE
 //��������� PLL 
 RCC->CFGR  |= RCC_CFGR_PLLSRC;                       //���������� ������� ��� PLL ������ HSE
 RCC->CR   &= ~RCC_CR_PLLON;                          //��������� ��������� PLL
 RCC->CFGR &= ~RCC_CFGR_PLLMULL;                      //�������� PLLMULL
 RCC->CFGR2 |=  RCC_CFGR2_PREDIV1SRC;                 //����� ������� �� PLL ��������� HSE
 RCC->CFGR2 &= ~RCC_CFGR2_PREDIV1;                    //������� ����� ������������ "PREDIV1"
 RCC->CFGR2 |=  RCC_CFGR2_PREDIV1_DIV5;               //���������� "PREDIV2" 
 RCC->CFGR2 &= ~RCC_CFGR2_PREDIV2;                    //������� ����� ������������ "PREDIV2"
 RCC->CFGR2 |=  RCC_CFGR2_PREDIV2_DIV5;               //���������� "PREDIV2" 
 RCC->CR   &= ~RCC_CR_PLL2ON;                         //��������� ��������� PLL2
 RCC->CFGR2 &= ~RCC_CFGR2_PLL2MUL;                    //������� ����� ������������ "PLL2MUL"
 RCC->CFGR2 |=  RCC_CFGR2_PLL2MUL8;                   //���������� "PLL2MUL"
 RCC->CR   &= ~RCC_CR_PLL3ON;                         //��������� ��������� PLL3
 RCC->CFGR2 &= ~RCC_CFGR2_PLL3MUL;                    //������� ����� ������������ "PLL3MUL"
 RCC->CFGR2 |=  RCC_CFGR2_PLL3MUL8;                   //���������� "PLL3MUL"
//
 RCC->CFGR |=  RCC_CFGR_PLLMULL6;                     //���������� ���������
 RCC->CR   |=  RCC_CR_PLL2ON;                         //�������� ��������� PLL2
  while((RCC->CR & RCC_CR_PLL2RDY)==0) {}             //�������� ���������� PLL2
 RCC->CR   |=  RCC_CR_PLLON;                          //�������� ��������� PLL
  while((RCC->CR & RCC_CR_PLLRDY)==0) {}              //�������� ���������� PLL
//
//
 //������������� �� ������������ �� PLL
 RCC->CFGR &= ~RCC_CFGR_SW;                           //������� ����� ������ ��������� ��������� �������
 RCC->CFGR |=  RCC_CFGR_SW_PLL;                       //������� ���������� ��������� ������� PLL
 while((RCC->CFGR&RCC_CFGR_SWS)!=0x08){}              //�������� ������������ �� PLL
//
 //��������� �������� ��� AHB
 RCC->CFGR &= ~RCC_CFGR_HPRE;                         //������� ����� ������������ "AHB Prescaler"
 RCC->CFGR |=  RCC_CFGR_HPRE_DIV2;                    //���������� "AHB Prescaler" 
//
  //��������� �������� ��� ���� APB1
  RCC->CFGR &= ~RCC_CFGR_PPRE1;                       //������� ����� ������������ "APB1 Prescaler"
  RCC->CFGR |=  RCC_CFGR_PPRE1_DIV1;                  //���������� "APB1 Prescaler" 
//
  //��������� �������� ��� ���� APB2
  RCC->CFGR &= ~RCC_CFGR_PPRE2;                       //������� ����� ������������ "APB2 Prescaler"
  RCC->CFGR |=  RCC_CFGR_PPRE2_DIV1;                  //���������� "APB2 Prescaler" 
//
//��������� �������� ��� ADC
  RCC->CFGR &= ~RCC_CFGR_ADCPRE;                      //������� ����� ������������ "ADC Prescaler"
  RCC->CFGR |=  RCC_CFGR_ADCPRE_DIV2;                 //���������� "ADC Prescaler" ������ 4
//
 //��������� �������� ��� ���� USB
 RCC->CFGR &= ~RCC_CFGR_OTGFSPRE;                     //������������ ��� USB ����� 2

}
}

void Init_EXTI(void) {
 
 RCC->APB2ENR |= RCC_APB2ENR_IOPDEN;
 RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
 

 GPIOD->CRH   &= ~GPIO_CRH_MODE13;                    //�������� ������� MODE
  GPIOD->CRH   &= ~GPIO_CRH_CNF13;                    //�������� ������� CNF
 GPIOD->CRH   |=  GPIO_CRH_CNF13_0;     //���������� ����, ������ ���������
 
 
 AFIO->EXTICR [13>>0x02] &= ~AFIO_EXTICR4_EXTI13_PD;
 AFIO->EXTICR [13>>0x02] |= AFIO_EXTICR4_EXTI13_PD; // ���������� INT13 �� PORTD. 
 
  EXTI->IMR|=EXTI_IMR_MR13;                            //��  �����
 EXTI->FTSR|=EXTI_FTSR_TR13;                          //�� ������
 EXTI->PR |= EXTI_PR_PR13; 
//NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);

 NVIC_EnableIRQ (EXTI15_10_IRQn);                     //�������� ����������
 NVIC_SetPriority(EXTI15_10_IRQn, 5);

}





