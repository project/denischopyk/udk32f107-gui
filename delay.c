/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "delay.h"



void Delay_ms(vu32 nCount)
{
  u16 i;
  for(; nCount != 0; nCount--)  for(i = 0; i < COU; ++i);
}

void Delay_us10(vu32 nCount)
{
  u16 i;
  for(; nCount != 0; nCount--)  for(i = 0; i < UCOU; ++i);
}

void Delay_us(vu32 nCount)
{
  u16 i;
  for(; nCount != 0; nCount--)  for(i = 0; i < UMCOU; ++i);
}