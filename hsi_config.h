
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HSI_CONFIG_H
#define __HSI_CONFIG_H

/* Includes ------------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Uncomment the line corresponding to the STMicroelectronics evaluation board
   used to run the example */



#define DELAY_COUNT    0xFFFFF

//#define HSI_Value    ((uint32_t)8000000) /*!< Value of the Internal oscillator in Hz*/
//#define HSE_Value    ((uint32_t)8000000) /*!< Value of the External oscillator in Hz*/

//#define SYSCLK_FREQ_HSE    HSE_Value
//#define SYSCLK_FREQ_20MHz  20000000
//#define SYSCLK_FREQ_36MHz  36000000
#define SYSCLK_FREQ_48MHz  48000000
//#define SYSCLK_FREQ_56MHz  56000000
//#define SYSCLK_FREQ_72MHz  72000000


#endif /* __HSI_CONFIG_H */

/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
